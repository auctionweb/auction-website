var express = require('express'),
    morgan = require('morgan'),
    handlebars = require('express-handlebars'),
    handlebars_sections = require('express-handlebars-sections'),
    bodyParser = require('body-parser'),
    session = require('express-session'),
    path = require('path');
var handle_layout = require('./middle-wares/handle-layout'),
    handle_404 = require('./middle-wares/handle-404'),
    homeController = require('./controllers/homeController'),
    auctionController = require('./controllers/auctionController'),
    sellerController = require('./controllers/sellerController'),
    adminController = require('./controllers/adminController'),
    dashboardController = require('./controllers/dashboardController');
var helper = require('./fn/helper'),
    constants = require("./fn/constants");
var mysql_store = require('express-mysql-session')(session);

//______________________________________________________________

var app = express();

//session store
var options = {
    host: '127.0.0.1',
    port: 3306,
    user: 'root',
    password: '',
    database: constants.DATABASE,
    checkExpirationInterval: 900000,
    expiration: 86400000,
    createDatabaseTable: true,
    connectionLimit: 1,
    schema: {
        tableName: 'SESSIONS',
        columnNames: {
            session_id: 'session_id',
            expires: 'expires',
            data: 'data'
        }
    }
};
var session_store = new mysql_store(options);
app.use(session({
    key: 'session_cookie_name',
    secret: 'session_cookie_secret',
    store: session_store,
    resave: false,
    saveUninitialized: false
}));

//set views folder
app.set('views', path.join(__dirname, '/views'));

//set view engine
app.engine('hbs', handlebars({
    extname: 'hbs',
    defaultLayout: 'main',
    layoutsDir: path.join(__dirname, '/views/_layouts/'),
    partialsDir: path.join(__dirname, '/views/_partials/'),
    helpers: {
        section: handlebars_sections(),
        formatCurrency: function(n) {
            return n.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,') + "$";
        }
    }
}));

app.set('view engine', 'hbs');

//body-parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));

//static resources
app.use(express.static(
    path.resolve(__dirname, 'public')

));

//morgan log
app.use(morgan('short'));

//controller
app.use(handle_layout);
app.use('/', homeController);
app.use('/auction', auctionController);
app.use('/seller', sellerController);
app.use('/dashboard', dashboardController);
app.use('/admin', adminController);
app.use(handle_404);

//create temp folder
helper.makeDir(constants.TEMP_DIR_PATH);
//______________________________________________________________
//listen 
app.listen(1234, function() {
    console.log('Server is running at port 1234!');
});