var express = require('express'),
    CryptoJS = require('crypto-js'),
    q = require('q'),
    mustache = require('mustache'),
    random = require('random-js');


var category = require('../models/category'),
    account = require('../models/account'),
    constants = require('../fn/constants'),
    account_sellrequest = require('../models/account-sellrequest'),
    helper = require('../fn/helper'),
    mailer = require('../fn/mailer');

var r = express.Router();
var nativeMath = random.engines.nativeMath;
//LOGIN FIRST && IS ADMIN
r.get('/:link', function(req, res, next) {
    //login first
    if (!req.session.userID) {
        return res.render('errors/require-login');
    }

    if (req.session.level != constants.ACCOUNT_TYPE_ADMIN) {
        return res.render('errors/error-page', {
            msg: "NEED LOGIN AS ADMIN!"
        });
    }

    next();
});

r.get('/account-info', function(req, res) {
    var vm = {
        account_info: true
    };

    var id = req.session.userID;
    account.loadDetail(id).then(function(account) {
        vm.userID = account['id'];
        vm.userEmail = account['email'];
        vm.verified = account['verified'];
        vm.userFullname = account['fullname'];
        vm.isGenderMale = (account['gender'] == constants.ACCOUNT_GENDER_MALE) ? true : false;
        vm.isSeller = (account['level'] == constants.ACCOUNT_TYPE_SELLER) ? true : false;
        if (vm.isSeller) vm.expired_date_string = account['expired-date-string'];
        var dob = account['dob'];
        if (dob) vm.userDOB = helper.formatDate(dob);
        vm.userPhone = account['phone-number'];
        vm.userAddress = account['address'];

        res.render('admin/admin-dashboard', vm);
    }).fail(function(err) {
        console.log(err);
        return res.render("errors/database-error");
    });

});

r.get('/manage-users', function(req, res) {
    account.loadAll()
        .then(function(rows) {
            var vm = {
                manage_users: true,
                account: rows
            };
            res.render('admin/admin-dashboard', vm);
        }).fail(function(err) {
            console.log(err);
            return res.render("errors/database-error");
        });
});

r.get('/manage-users/resetPassword', function(req, res) {
    var id = req.query.id;
    account.loadDetail(id).then(function(acc) {
        var vm = {
            account: acc
        };
        res.render('admin/manage-users/resetPassword', vm);
    });
});

r.post('/manage-users/resetPassword', function(req, res) {

    var temp = random.integer(100000, 999999);
    var newPassword = temp(nativeMath);
    var account_id = req.body["account-id"];
    var options = {
        subject: "YOUR PASSWORD HAS BEEN RESET",
        text: "",
        html: ""
    }
    account.getEmail(account_id).then(function(mail) {
        options.to = mail["email"];
        options.text = "your new password:" + newPassword;
    })

    q.all([
            account.updatePassword(account_id, newPassword),
        ])
        .spread(function(changedRows) {
            mailer.sendMail(options);
            res.redirect('/admin/manage-users');
        }).fail(function(err) {
            console.log(err);
            return res.render("errors/database-error");
        });
});


r.post('/manage-users/delete', function(req, res) {
    account.delete(req.body).then(function(affectedRows) {
        return res.redirect('/admin/manage-users');
    }).fail(function(err) {
        console.log(err);
        return res.render("errors/database-error");
    });
});




r.get('/manage-category', function(req, res) {
    category.loadAll()
        .then(function(rows) {
            var vm = {
                manage_category: true,
                categories: rows
            };
            res.render('admin/admin-dashboard', vm);
        }).fail(function(err) {
            console.log(err);
            return res.render("errors/database-error");
        });
});

r.get('/manage-category/add', function(req, res) {
    var vm = {
        //manage_category:true
    }
    res.render('admin/manage-category/add', vm);
});
r.post('/manage-category/add', function(req, res) {
    category.insert(req.body).then(function(data) {
        var vm = {};
        res.render('admin/manage-category/add', vm);
    }).fail(function(err) {
        console.log(err);
        return res.render("errors/database-error");
    });
});
r.get('/manage-category/edit', function(req, res) {
    var id = req.query.id;
    category.loadDetail(id).then(function(cat) {
        var vm = {
            category: cat
        };
        res.render('admin/manage-category/edit', vm);
    }).fail(function(err) {
        console.log(err);
        return res.render("errors/database-error");
    });
});
r.post('/manage-category/edit', function(req, res) {
    category.update(req.body).then(function(changedRows) {
        res.redirect('/admin/manage-category');
    }).fail(function(err) {
        console.log(err);
        return res.render("errors/database-error");
    });
});
r.post('/manage-category/delete', function(req, res) {
    category.delete(req.body).then(function(affectedRows) {
        res.redirect('/admin/manage-category');
    }).fail(function(err) {
        console.log(err);
        return res.render("errors/database-error");
    });
});


r.get('/request', function(req, res) {
    account_sellrequest.loadAll()
        .then(function(rows) {
            var vm = {
                request: true,
                account_sellrequest: rows
            };
            res.render('admin/admin-dashboard', vm);
        }).fail(function(err) {
            console.log(err);
            return res.render("errors/database-error");
        });
});

r.post('/acceptRequest', function(req, res) {
    account_sellrequest.updateToSellAccount(req.body).then(function(changedRows) {
        res.redirect('/admin/request');
    }).fail(function(err) {
        console.log(err);
        return res.render("errors/database-error");
    });

});

r.post('/rejectRequest', function(req, res) {
    account_sellrequest.rejectRequest(req.body).then(function(changedRows) {
        res.redirect('/admin/request');
    }).fail(function(err) {
        console.log(err);
        return res.render("errors/database-error");
    });
});
module.exports = r;