var express = require('express'),
	q = require('q');
var auctionRepo = require('../models/auction'),
	feedbackRepo = require('../models/feedback'),
	constants = require('../fn/constants'),
	helper = require('../fn/helper');

var r = express.Router();
// /id
r.get('/:id', function(req, res) {
	var vm = {};

	var acc_id = req.params.id;
	vm.userID = acc_id;
	q.all([
		helper.getAccountPointInfo(acc_id),
		feedbackRepo.loadAll_ByToAccountID_OrderByDateDESC(acc_id),
		auctionRepo.loadAll_SellingList(acc_id)
	]).spread(function(info, feedbacks, auctions) {
		console.log(info);
		vm.plus_point = info.num_of_plus * constants.POINT_PLUS;
		vm.minus_point = info.num_of_minus * constants.POINT_MINUS;
		vm.total_point = vm.plus_point - vm.minus_point;
		vm.percent = info.percent;
		vm.num_of_feedback = info.num_of_all;


		vm.feedbacks = [];
		vm.total_feedbacks = feedbacks.length;
		for (var i = 0; i < feedbacks.length; i++) {
			var cell = {};
			cell.isPlus = (feedbacks[i]['point'] > 0) ? true : false;
			cell.isWinner = (feedbacks[i]['from-type'] == constants.FROM_TYPE_WINNER) ? true : false;
			cell.cat_id = feedbacks[i]['cat-id'];
			cell.auc_id = feedbacks[i]['auction-id'];
			cell.from_id = feedbacks[i]['from'];
			cell.comment = feedbacks[i]['comment'];
			cell.auc_title = feedbacks[i]['auction-title'];
			cell.date_string = feedbacks[i]['date-string'];

			vm.feedbacks.push(cell);
		}

		helper.parseInfoForListAuctionView(auctions, acc_id, 100, 1000).then(function(cells) {
			vm.total_auctions = cells.length;
			vm.auctions = cells;
			res.render('seller/index', vm);
		}).fail(function(err) {
			console.log(err);
			return res.render("errors/database-error");
		});
	}).fail(function(err) {
		console.log(err);
		return res.render("errors/database-error");
	});
});
module.exports = r;