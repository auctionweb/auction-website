var express = require('express'),
	q = require('q');
var accountRepo = require('../models/account'),
	auctionRepo = require('../models/auction'),
	sellrequestRepo = require('../models/account-sellrequest'),
	feedbackRepo = require('../models/feedback'),
	watchlistRepo = require('../models/watchlist'),
	constants = require('../fn/constants'),
	helper = require('../fn/helper');

var r = express.Router();

//LOGIN FIRST
r.get('/:link', function(req, res, next) {
	//login first
	if (!req.session.userID) {
		return res.render('errors/require-login');
	}

	next();
});


//ACCOUNT INFO - DONE
r.get('/account-info', function(req, res) {
	var vm = {
		account_info: true
	};

	var id = req.session.userID;
	accountRepo.loadDetail(id).then(function(account) {
		vm.userID = account['id'];
		vm.userEmail = account['email'];
		vm.verified = account['verified'];
		vm.userFullname = account['fullname'];
		vm.isGenderMale = (account['gender'] == constants.ACCOUNT_GENDER_MALE) ? true : false;
		vm.isSeller = (account['level'] == constants.ACCOUNT_TYPE_SELLER) ? true : false;
		if (vm.isSeller) vm.expired_date_string = account['expired-date-string'];
		var dob = account['dob'];
		if (dob) vm.userDOB = helper.formatDate(dob);
		vm.userPhone = account['phone-number'];
		vm.userAddress = account['address'];

		res.render('dashboard/dashboard', vm);
	}).fail(function(err) {
		console.log(err);
		return res.render("errors/database-error");
	});
});
r.post('/account-info/update-basic-info', function(req, res) {
	var data = {};
	data.success = false;

	//login first
	if (!req.session.userID) {
		data.needLogin = true;
		return res.json(data);
	}

	//check form format
	var form = req.body;
	if (form["fullname"] === undefined || form["gender"] === undefined ||
		form["dob"] === undefined || form["phone"] === undefined ||
		form["address"] === undefined) {
		data.msg = 'Form data is not correct format.';
		return res.json(data);
	}

	var info = {
		fullname: helper.formatString(form['fullname']),
		gender: form['gender'],
		dob: form['dob'],
		'phone-number': form['phone'],
		address: helper.formatString(form['address']),
		id: req.session.userID
	};

	accountRepo.updateBasicInfo(info)
		.then(function(rlt) {
			data.success = true;
			req.session.fullname = info.fullname;
			return res.json(data);
		})
		.fail(function(err) {
			console.log(err);
			data.msg = 'Error happened!';
			return res.json(data);
		});
});
r.post('/account-info/update-email', function(req, res) {
	var data = {};
	data.success = false;

	//login first
	if (!req.session.userID) {
		data.needLogin = true;
		return res.json(data);
	}

	//check form format
	var form = req.body;
	if (form["email"] === undefined) {
		data.msg = 'Form data is not correct format.';
		return res.json(data);
	}

	accountRepo.updateEmail(req.session.userID, form["email"])
		.then(function(rlt) {
			data.success = true;
			return res.json(data);
		})
		.fail(function(err) {
			console.log(err);
			if (err.code == 'ER_DUP_ENTRY') {
				data.msg = 'Email "' + form['email'] + '" has been used! Try another!';
			} else data.msg = "Error happened!";

			return res.json(data);
		});
});
r.post('/account-info/update-password', function(req, res) {
	var data = {};
	data.success = false;

	//login first
	if (!req.session.userID) {
		data.needLogin = true;
		return res.json(data);
	}

	//check form format
	var form = req.body;
	if (form["current_password"] === undefined || form["new_password"] === undefined) {
		data.msg = 'Form data is not correct format.';
		return res.json(data);
	}

	var acc_id = req.session.userID;
	accountRepo.checkIfPasswordCorrect(acc_id, form["current_password"]).then(function(rlt) {
		if (!rlt) {
			console.log("HERRE!");
			data.msg = "Wrong password!";
			return res.json(data);
		} else {
			console.log("HERRE111!");
			accountRepo.updatePassword(acc_id, form["new_password"]).then(function() {
				data.success = true;
				return res.json(data);
			}).fail(function(err) {
				console.log(err);
				data.msg = 'Error happened!';
				return res.json(data);
			})
		}

	}).fail(function(err) {
		console.log(err);
		data.msg = 'Error happened!';
		return res.json(data);
	});
});
r.post('/account-info/sell-request', function(req, res) {
	var data = {};
	data.success = false;

	//login first
	if (!req.session.userID) {
		data.needLogin = true;
		return res.json(data);
	}

	//check form format
	var form = req.body;
	if (form["content"] === undefined) {
		data.msg = 'Form data is not correct format.';
		return res.json(data);
	}


	var now = new Date().toISOString().slice(0, 19).replace('T', ' ');
	var info = {
		'account-id': req.session.userID,
		'date': now,
		'content': helper.formatString(form['content']),
		'status': constants.SELLREQUEST_STATUS_WAITING
	};

	sellrequestRepo.insert(info).then(function() {
		data.success = true;
		return res.json(data);
	}).fail(function(err) {
		console.log(err);
		data.msg = "Error happened!";
		return res.json(data);
	});
});

//FEEDBACK INFO - DONE
r.get('/feedback-info', function(req, res) {
	var vm = {
		feedback_info: true
	};

	var acc_id = req.session.userID;
	q.all([
		helper.getAccountPointInfo(acc_id)
	]).spread(function(info) {
		vm.plus_point = info.num_of_plus * constants.POINT_PLUS;
		vm.minus_point = info.num_of_minus * constants.POINT_MINUS;
		vm.total_point = vm.plus_point - vm.minus_point;
		vm.percent = info.percent;
		vm.num_of_feedback = info.num_of_all;

		feedbackRepo.loadAll_ByToAccountID_OrderByDateDESC(acc_id).then(function(rows) {
			vm.feedbacks = [];
			for (var i = 0; i < rows.length; i++) {
				var cell = {};
				cell.isPlus = (rows[i]['point'] > 0) ? true : false;
				cell.isWinner = (rows[i]['from-type'] == constants.FROM_TYPE_WINNER) ? true : false;
				cell.cat_id = rows[i]['cat-id'];
				cell.auc_id = rows[i]['auction-id'];
				cell.from_id = rows[i]['from'];
				cell.comment = rows[i]['comment'];
				cell.auc_title = rows[i]['auction-title'];
				cell.date_string = rows[i]['date-string'];

				vm.feedbacks.push(cell);
			}

			res.render('dashboard/dashboard', vm);
		}).fail(function(err) {
			console.log(err);
			return res.render("errors/database-error");
		});

	}).fail(function(err) {
		console.log(err);
		return res.render("errors/database-error");
	});
});

//WATCH LIST - DONE
r.get('/watch-list', function(req, res) {
	var vm = {
		watch_list: true
	};
	var acc_id = req.session.userID;
	auctionRepo.loadAll_WatchList(acc_id).then(function(rows) {
		helper.parseInfoForListAuctionView(rows, req.session.userID, 100, 1000).then(function(cells) {
			vm.total = cells.length;
			vm.auctions = cells;
			res.render('dashboard/dashboard', vm);
		}).fail(function(err) {
			console.log(err);
			return res.render("errors/database-error");
		});

	}).fail(function(err) {
		console.log(err);
		return res.render("errors/database-error");
	});
});
r.post('/watch-list/remove/:aucID', function(req, res) {
	var data = {};
	data.success = false;

	//login first
	if (!req.session.userID) {
		data.needLogin = true;
		return res.json(data);
	}

	//check form format
	var form = req.body;
	if (form["remove-from-watchlist"] === undefined) {
		data.msg = 'Form data is not correct format.';
		return res.json(data);
	}

	var auc_id = req.params.aucID;
	var acc_id = req.session.userID;
	watchlistRepo.delete(auc_id, acc_id)
		.then(function(rlt) {
			data.success = true;
			return res.json(data);
		})
		.fail(function(err) {
			console.log(err);
			data.msg = 'Error happened!';
			return res.json(data);
		});
});

//WIN LIST - DONE
r.get('/win-list', function(req, res) {
	var vm = {
		win_list: true,
		hasfeedback: true,
	};

	var acc_id = req.session.userID;
	auctionRepo.loadAll_WinList(acc_id).then(function(rows) {
		q.all([
			helper.parseInfoForListAuctionView(rows, req.session.userID, 100, 1000),

		]).spread(function(cells) {
			vm.total = cells.length;
			vm.auctions = cells;
			res.render('dashboard/dashboard', vm);
		}).fail(function(err) {
			console.log(err);
			return res.render("errors/database-error");
		});

	}).fail(function(err) {
		console.log(err);
		return res.render("errors/database-error");
	});
});
r.post('/win-list/feedback-to-seller/:sellerID/:aucID/', function(req, res) {
	var data = {};
	data.success = false;

	//login first
	if (!req.session.userID) {
		data.needLogin = true;
		return res.json(data);
	}

	//check form format
	var form = req.body;
	if (form["point"] === undefined || form["comment"] === undefined) {
		data.msg = 'Form data is not correct format.';
		return res.json(data);
	}

	var auc_id = req.params.aucID;
	var acc_id = req.session.userID;
	var info = {
		'auction-id': req.params.aucID,
		'from': req.session.userID,
		'to': req.params.sellerID,
		'comment': helper.formatString(form['comment']),
		'point': parseInt(form['point']),
		'from-type': constants.FROM_TYPE_WINNER,
		'date': new Date()
	};

	feedbackRepo.insert(info)
		.then(function(rlt) {
			console.log(rlt);
			data.success = true;
			return res.json(data);
		})
		.fail(function(err) {
			console.log(err);
			if (err.code == 'ER_DUP_ENTRY') {
				data.success = false;
				data.msg = "Send feedback fail! You have already sent!";
			} else data.msg = "Error happened!";
			return res.json(data);
		});
});

//SOLD LIST - DONE
r.get('/sold-list', function(req, res) {
	var vm = {
		sold_list: true,
		hasfeedback: true,
	};
	var acc_id = req.session.userID;
	auctionRepo.loadAll_SoldList(acc_id).then(function(rows) {
		q.all([
			helper.parseInfoForListAuctionView(rows, req.session.userID, 100, 1000),

		]).spread(function(cells) {
			vm.total = cells.length;
			vm.auctions = cells;
			res.render('dashboard/dashboard', vm);
		}).fail(function(err) {
			console.log(err);
			return res.render("errors/database-error");
		});

	}).fail(function(err) {
		console.log(err);
		return res.render("errors/database-error");
	});
});
r.post('/sold-list/feedback-to-winner/:winnerID/:aucID/', function(req, res) {
	var data = {};
	data.success = false;

	//login first
	if (!req.session.userID) {
		data.needLogin = true;
		return res.json(data);
	}

	//check form format
	var form = req.body;
	if (form["point"] === undefined || form["comment"] === undefined) {
		data.msg = 'Form data is not correct format.';
		return res.json(data);
	}

	var auc_id = req.params.aucID;
	var acc_id = req.session.userID;
	var info = {
		'auction-id': req.params.aucID,
		'from': req.session.userID,
		'to': req.params.winnerID,
		'comment': helper.formatString(form['comment']),
		'point': parseInt(form['point']),
		'from-type': constants.FROM_TYPE_SELLER,
		'date': new Date()
	};

	feedbackRepo.insert(info)
		.then(function(rlt) {
			console.log(rlt);
			data.success = true;
			return res.json(data);
		})
		.fail(function(err) {
			console.log(err);
			if (err.code == 'ER_DUP_ENTRY') {
				data.success = false;
				data.msg = "Send feedback fail! You have already sent!";
			} else data.msg = "Error happened!";
			return res.json(data);
		});
});

//SELLING LIST - DONE
r.get('/selling-list', function(req, res) {
	var vm = {
		selling_list: true
	};

	var acc_id = req.session.userID;
	auctionRepo.loadAll_SellingList(acc_id).then(function(rows) {
		q.all([
			helper.parseInfoForListAuctionView(rows, req.session.userID, 100, 1000),

		]).spread(function(cells) {
			vm.total = cells.length;
			vm.auctions = cells;
			res.render('dashboard/dashboard', vm);
		}).fail(function(err) {
			console.log(err);
			return res.render("errors/database-error");
		});

	}).fail(function(err) {
		console.log(err);
		return res.render("errors/database-error");
	});
});

//BIDDING LIST - XXX
r.get('/bidding-list', function(req, res) {
	var vm = {
		bidding_list: true
	};
	var acc_id = req.session.userID;
	auctionRepo.loadAll_BiddingList(acc_id).then(function(rows) {
		helper.parseInfoForListAuctionView(rows, req.session.userID, 100, 1000).then(function(cells) {
			vm.total = cells.length;
			vm.auctions = cells;
			res.render('dashboard/dashboard', vm);
		}).fail(function(err) {
			console.log(err);
			return res.render("errors/database-error");
		});

	}).fail(function(err) {
		console.log(err);
		return res.render("errors/database-error");
	});
});


module.exports = r;