var express = require('express'),
	mustache = require('mustache'),
	q = require('q'),
	multer = require('multer'),
	cloudinary = require('cloudinary'),
	path = require('path'),
	fs = require('fs'),
	querystring = require('querystring');
var auctionRepo = require('../models/auction'),
	accountRepo = require('../models/account'),
	blacklistRepo = require('../models/blacklist'),
	bidloggerRepo = require('../models/bidlogger'),
	watchlistRepo = require('../models/watchlist'),
	categoryRepo = require('../models/category'),
	constants = require('../fn/constants'),
	helper = require('../fn/helper'),
	system = require('../fn/system');


var r = express.Router();

//DONE
r.get('/create', function(req, res) {
	//login first
	if (!req.session.userID) {
		return res.render('errors/require-login');
	}

	accountRepo.checkIfCanCreateAuction(req.session.userID).then(function(rlt) {
		if (rlt) return res.render('auction/create-auction');
		else return res.render('errors/error-page', {
			msg: "Your account can't create auction! Please send sell request at profile page!"
		});
	}).fail(function(err) {
		console.log(err);
		return res.render("errors/database-error");
	});

});
//DONE
r.post('/create/response', function(req, res) {
	//login first
	if (!req.session.userID) {
		return res.render('errors/require-login');
	}

	var vm = {};

	accountRepo.checkIfCanCreateAuction(req.session.userID).then(function(rlt) {
		if (!rlt) return res.render('errors/error-page', {
			msg: "Your account can't create auction! Please send sell request at profile page!"
		});
		helper.createDefaultAuction()
			.then(function(id) {
				//upload image
				helper.uploadToCDN(req, res, id).then(function() {
					//check form data format
					var form = req.body;
					console.log("FORM: " + JSON.stringify(form));
					if (!(form.auc_title && form.auc_desc && form.date_end_string && form.price_start &&
							form.bid_increment && form.has_buyout_price && form.price_buyout && form.auc_category)) {
						return res.render('errors/form-format-error');
					}
					//update auction info
					var info = helper.parseCreateAuctionFormToInfo(req);
					info.id = id;

					auctionRepo.update(info)
						.then(function() {
							vm.isSuccess = true;
							return res.render('auction/create-auction-response', vm);
						})
						.fail(function(err) {
							console.log(err);
							auctionRepo.delete(info);
							return res.render("errors/database-error");
						});

				}).fail(function(err) {
					vm.isSuccess = false;
					auctionRepo.delete(info);
					console.log(err);
					return res.render('auction/create-auction-response', vm);
				});
			})
			.fail(function(err) {
				console.log("IN CREATE DEFAULT: " + err);
				return res.render("errors/database-error");
			});
	}).fail(function(err) {
		console.log(err);
		return res.render("errors/database-error");
	});

});


//DONE
r.get('/:catID', function(req, res) {
	var vm = {};

	var cat_id = req.params.catID;
	var temp = parseInt(req.query.page);
	var current_page = (!temp || temp <= 0) ? 1 : parseInt(req.query.page);

	var load_promise,
		promises = [];
	promises.push(categoryRepo.loadDetail(cat_id));

	var q_str = req.query.q;
	var q_order = req.query.order;
	vm.order_string = querystring.escape(q_order);

	if (!q_str) {
		vm.onlyStringOrCategorySearch = true;
		vm.onlyCategorySearch = true;
		promises.push(auctionRepo.countAll_ByCatID(cat_id));

		switch (q_order) {
			case constants.ORDER_END_ASC:
				vm.order = "Time end: soon to late";
				load_promise = auctionRepo.loadLimit_ByCatID_EndDateASC(cat_id,
					(current_page - 1) * constants.NUM_AUCTION_PER_PAGE, constants.NUM_AUCTION_PER_PAGE);
				break;
			case constants.ORDER_END_DESC:
				vm.order = "Time end: late to soon";
				load_promise = auctionRepo.loadLimit_ByCatID_EndDateDESC(cat_id,
					(current_page - 1) * constants.NUM_AUCTION_PER_PAGE, constants.NUM_AUCTION_PER_PAGE);
				break;
			case constants.ORDER_PRICE_ASC:
				vm.order = "Curren price: low to high";
				load_promise = auctionRepo.loadLimit_ByCatID_PriceASC(cat_id,
					(current_page - 1) * constants.NUM_AUCTION_PER_PAGE, constants.NUM_AUCTION_PER_PAGE);
				break;
			case constants.ORDER_PRICE_DESC:
				vm.order = "Curren price: high to low";
				load_promise = auctionRepo.loadLimit_ByCatID_PriceDESC(cat_id,
					(current_page - 1) * constants.NUM_AUCTION_PER_PAGE, constants.NUM_AUCTION_PER_PAGE);
				break;
			default:
				vm.order_default = true;
				load_promise = auctionRepo.loadLimit_ByCatID(cat_id,
					(current_page - 1) * constants.NUM_AUCTION_PER_PAGE, constants.NUM_AUCTION_PER_PAGE);
		}
	} else {
		vm.onlyStringOrCategorySearch = false;
		vm.hasQueryString = true;
		vm.query_string = querystring.escape(q_str).replace(/%20/g, "+");
		var words = helper.convertStringToWords(q_str);

		promises.push(auctionRepo.countAll_TitleContainsWords_ByCatID(words, cat_id));

		switch (q_order) {
			case constants.ORDER_END_ASC:
				vm.order = "Time end: soon to late";
				load_promise = auctionRepo.loadLimit_TitleContainsWords_ByCatID_EndDateASC(words, cat_id,
					(current_page - 1) * constants.NUM_AUCTION_PER_PAGE, constants.NUM_AUCTION_PER_PAGE);
				break;
			case constants.ORDER_END_DESC:
				vm.order = "Time end: late to soon";
				load_promise = auctionRepo.loadLimit_TitleContainsWords_ByCatID_EndDateDESC(words, cat_id,
					(current_page - 1) * constants.NUM_AUCTION_PER_PAGE, constants.NUM_AUCTION_PER_PAGE);
				break;
			case constants.ORDER_PRICE_ASC:
				vm.order = "Curren price: low to high";
				load_promise = auctionRepo.loadLimit_TitleContainsWords_ByCatID_PriceASC(words, cat_id,
					(current_page - 1) * constants.NUM_AUCTION_PER_PAGE, constants.NUM_AUCTION_PER_PAGE);
				break;
			case constants.ORDER_PRICE_DESC:
				vm.order = "Curren price: high to low";
				load_promise = auctionRepo.loadLimit_TitleContainsWords_ByCatID_PriceDESC(words, cat_id,
					(current_page - 1) * constants.NUM_AUCTION_PER_PAGE, constants.NUM_AUCTION_PER_PAGE);
				break;
			default:
				vm.order_default = true;
				load_promise = auctionRepo.loadLimit_TitleContainsWords_ByCatID(words, cat_id,
					(current_page - 1) * constants.NUM_AUCTION_PER_PAGE, constants.NUM_AUCTION_PER_PAGE);
		}
	}

	q.all(promises).spread(function(cat, totalRec) {
		//cat
		vm.cat_id = cat["id"];
		vm.cat_name = cat["name"];
		if (cat === undefined) return res.render('errors/404-error');
		vm.result_name = cat["name"];

		if (!vm.onlyStringOrCategorySearch) {
			vm.result_name = helper.formatString(q_str);
		}

		//totalRec
		var num_page = Math.ceil(totalRec / constants.NUM_AUCTION_PER_PAGE);

		vm.result_count = totalRec;
		if (num_page <= 1) vm.hidePageNav = true;
		if (totalRec <= 0 || current_page > num_page) {
			vm.isEmpty = true;
			return res.render('auction/list-auction', vm);
		} else {
			vm.pages = [];
			for (var i = 1; i <= num_page; i++) {
				vm.pages.push({
					num: i,
					isActive: false
				});
			}
			vm.pages[current_page - 1].isActive = true;


			load_promise.then(function(rows) {
				if (rows.length <= 0) vm.isEmpty = true;

				helper.parseInfoForListAuctionView(rows, req.session.userID, 1000, constants.IMAGE_HEIGHT_AUCTION_CELL).then(function(cells) {
					vm.auctions = cells;
					console.log(cells);
					return res.render('auction/list-auction', vm);
				}).fail(function(err) {
					console.log(err);
					return res.render('errors/database-error');
				});
			}).fail(function(err) {
				console.log(err);
				return res.render('errors/database-error');
			});
		}

	}).fail(function(err) {
		console.log(err);
		return res.render('errors/database-error');
	});
});

// auction-detail - DONE
r.get('/:catID/:aucID', function(req, res) {

	var vm = {};
	auctionRepo.loadDetail(req.params.aucID)
		.then(function(rlt) {
			if (rlt === undefined) return res.render('errors/404-error');
			if (rlt["cat-id"] != req.params.catID) {
				console.log("CATID & AUCID NOT RIGHT");
				return res.render('errors/404-error');
			}

			vm.auc_id = rlt["id"];
			vm.cat_id = rlt["cat-id"];
			vm.cat_name = res.locals.layoutVM.categories.find(c => c.id == req.params.catID).name;
			vm.auc_title = rlt["title"];
			vm.auc_description = rlt["description"];
			vm.current_price = rlt["current-price"];
			vm.has_buyout = rlt["has-buyout"];
			vm.has_winner = rlt["has-winner"];
			vm.buyout_price = rlt["buyout-price"];
			vm.bid_increment = rlt["bid-increment"];
			vm.min_bid_price = parseFloat(vm.current_price) + parseFloat(vm.bid_increment);
			vm.start_date_string = rlt["start-date-string"];
			vm.end_date_string = rlt["end-date-string"];
			vm.seller_id = rlt["seller-id"];
			vm.isOwner = false;
			if (req.session.userID == vm.seller_id) {
				vm.isOwner = true;
			}

			q.all([
				helper.getListHTMLImage(vm.auc_id, 1000, constants.IMAGE_HEIGHT_AUCTION_DETAIL),
				helper.countAccountTotalPoint(vm.seller_id),
				helper.getAuctionBidNumber(vm.auc_id),
				helper.countAccountTotalPoint(rlt["highest-id"]),
				watchlistRepo.checkIfExist(vm.auc_id, req.session.userID)
			]).spread(function(imgs, point1, bids, point2, isInWatchlist) {

				// if (imgs.length > 0) vm.auc_img1 = imgs[0];
				vm.auc_imgs = [];
				for (var i = 0; i < imgs.length; i++) {
					var obj = {};
					obj.index = i;
					obj.content = imgs[i];
					obj.isActive = false;
					vm.auc_imgs.push(obj);

					if (i == 0) vm.auc_imgs[0].isActive = true;
				}

				vm.seller_point = point1;

				vm.auc_bids = bids;
				vm.has_bidder = false;
				if (bids > 0) {
					vm.has_bidder = true;
					vm.auc_highest = helper.encodeBidderID(rlt["highest-id"]);
					vm.auc_highest_full = rlt["highest-id"];
					vm.auc_highest_point = point2;
				}

				vm.isInWatchlist = isInWatchlist;
				return res.render('auction/auction-detail', vm);
			}).fail(function(err) {
				console.log("BID INFO: " + err);
				return res.render('errors/404-error');
			});

		})
		.fail(function(err) {
			console.log("AUCTION LOAD: " + err);
			return res.render('errors/404-error');
		});

});

//add description - DONE
r.post('/:catID/:aucID/edit', function(req, res) {
	var data = {};
	data.success = false;

	//login first
	if (!req.session.userID) {
		data.needLogin = true;
		return res.json(data);
	}


	//check form format
	var form = req.body;
	if (!(form['add_content'])) {
		data.msg = 'Form data is not correct format.';
		return res.json(data);
	}

	var auc_id = req.params.aucID;
	var acc_id = req.session.userID;
	//check is owner of auction
	auctionRepo.checkIsAuctionOwner(auc_id, acc_id)
		.then(function(rlt) {
			if (!rlt) {
				data.msg = 'You are not owner of this Auction.';
				return res.json(data);
			}

			auctionRepo.updateDescription(auc_id, form.add_content)
				.then(function(changedRows) {
					if (changedRows <= 0) {
						data.msg = 'Update not success!';
						return res.json(data);
					}
					data.success = true;
					return res.json(data);
				})
				.fail(function() {
					console.log(err);
					data.msg = 'database error';
					return res.json(data);
				});

		})
		.fail(function(err) {
			console.log(err);
			data.msg = 'database error';
			return res.json(data);
		});
});
//bid - DONE
r.post('/:catID/:aucID/bid', function(req, res) {
	var data = {};
	data.success = false;

	//login first
	if (!req.session.userID) {
		data.needLogin = true;
		return res.json(data);
	}

	//check form format
	var form = req.body;
	if (!(form.bid_price)) {
		data.msg = 'Form data is not correct format.';
		return res.json(data);
	}

	var auc_id = req.params.aucID;
	var acc_id = req.session.userID;

	q.all([
		helper.getAccountPointInfo(acc_id),
		blacklistRepo.checkIfExist(auc_id, acc_id)
	]).spread(function(info, rlt2) {
		if (info.percent < constants.MIN_PERCENT_CAN_BID) {
			data.msg = "Your feedback point doesn't meet bid condition!";
			return res.json(data);
		}

		if (rlt2) {
			data.msg = "You are in blacklist of this auction!";
			return res.json(data);
		}

		helper.bidAuction(auc_id, acc_id, form.bid_price).then(function(rlt) {
			system.handleMakeBidSuccessEvent(auc_id);
			if (!rlt.success) {
				if (rlt.hasHigher) data.msg = "Some people keep the higher price!";
				else data.msg = "Error happened!";
			} else {
				data.success = true;
			}
			return res.json(data);
		}).fail(function(err) {
			console.log(err);
			data.msg = "Error happened!";
			return res.json(data);
		});
	}).fail(function(err) {
		console.log(err);
		data.msg = "Error happened!";
		return res.json(data);
	});

});
//buyout - DONE
r.post('/:catID/:aucID/buy', function(req, res) {
	var data = {};
	data.success = false;

	//login first
	if (!req.session.userID) {
		data.needLogin = true;
		return res.json(data);
	}

	//check form format
	var form = req.body;
	if (!form.buynow) {
		data.msg = 'Form data is not correct format.';
		return res.json(data);
	}

	var auc_id = req.params.aucID;
	var acc_id = req.session.userID;
	q.all([
		helper.getAccountPointInfo(acc_id),
		blacklistRepo.checkIfExist(auc_id, acc_id)
	]).spread(function(info, rlt2) {
		if (info.percent < constants.MIN_PERCENT_CAN_BID) {
			data.msg = "Your feedback point doesn't meet bid condition!";
			return res.json(data);
		}

		if (rlt2) {
			data.msg = "You are in blacklist of this auction!";
			return res.json(data);
		}

		helper.buyAuctionNow(auc_id, req.session.userID).then(function(rlt) {
			if (rlt == false) {
				data.msg = 'Buy fail!';
				return res.json(data);
			}
			system.handleBuySuccessEvent(auc_id);
			data.success = true;
			return res.json(data);
		}).fail(function(err) {
			console.log(err);
			data.msg = 'Error happened!';
			return res.json(data);
		});

	}).fail(function(err) {
		console.log(err);
		data.msg = "Error happened!";
		return res.json(data);
	});
});

//add to watchlist - DONE
r.post('/:catID/:aucID/add-to-watchlist', function(req, res) {
	var data = {};
	data.success = false;

	//login first
	if (!req.session.userID) {
		console.log("Chua login");
		data.needLogin = true;
		return res.json(data);
	}

	//check form format
	var form = req.body;
	if (!form["add-to-watchlist"]) {
		data.msg = 'Form data is not correct format.';
		return res.json(data);
	}

	var auc_id = req.params.aucID;
	var acc_id = req.session.userID;
	watchlistRepo.insert(auc_id, acc_id)
		.then(function(rlt) {
			data.success = true;
			return res.json(data);
		})
		.fail(function(err) {
			console.log(err);
			data.msg = 'Error happened!';
			return res.json(data);
		});
});

//bid history - DONE
r.get('/:catID/:aucID/auction-bidlog', function(req, res) {

	var acc_id = req.session.userID,
		auc_id = req.params.aucID,
		cat_id = req.params.catID;

	q.all([
		bidloggerRepo.loadAll_ByAuctionID_OrderByLogIDDESC(auc_id),
		auctionRepo.checkIsAuctionOwner(auc_id, acc_id),
		auctionRepo.loadDetail(auc_id)
	]).spread(function(rlt, isOwner, auction) {
		var vm = {};
		vm.auc_id = auc_id;
		vm.cat_id = cat_id;
		vm.auc_title = auction['title'];
		vm.isOwner = isOwner;
		vm.bidlogger = rlt;
		if (!vm.isOwner) {
			for (var i = 0; i < rlt.length; i++) {
				vm.bidlogger[i]["account-id"] = helper.encodeBidderID(rlt[i]["account-id"]);
			}
		};

		res.render('auction/auction-bidlog', vm);
	}).fail(function() {
		console.log(err);
		return res.render("errors/database-error");
	});
});

r.post('/:catID/:aucID/auction-bidlog/delete-account-bid', function(req, res) {
	var cat_id = req.params.catID;
	var auc_id = req.params.aucID;

	var account_id = req.body["account-id"],
		bid_id = req.body['bid-id'];

	q.all([
		auctionRepo.loadDetail(auc_id),
		blacklistRepo.insert(auc_id, account_id),
		bidloggerRepo.loadDetail(bid_id),
		bidloggerRepo.loadHighestDetail(auc_id),
	]).spread(function(auction, insertRlt, bidlog, bidlog_highest) {
		// console.log("IIIII");
		// console.log("IIIIIauction: " + JSON.stringify(auction));
		// console.log("IIIIIlog: " + JSON.stringify(biglog));
		// console.log("IIIIIhighest log: " + JSON.stringify(bidlog_highest));
		// console.log("IIIIIinsert: " + insertRlt);
		var isHighest = false;
		if (bidlog['account-id'] == bidlog_highest['account-id']) isHighest = true;
		system.handleKickOutEvent(auc_id, account_id, isHighest).then(function(rlt) {
			console.log("IIIII---" + rlt);
			return res.redirect('/auction/' + cat_id + '/' + auc_id + '/auction-bidlog');
		}).fail(function(err) {
			console.log(err);
			return res.render("errors/database-error");
		});

	}).fail(function(err) {
		console.log(err);
		return res.render("errors/database-error");
	});


});


module.exports = r;