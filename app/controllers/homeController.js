var express = require('express'),
	request = require('request'),
	q = require('q'),
	querystring = require('querystring');
var accountRepo = require('../models/account'),
	auctionRepo = require('../models/auction'),
	categoryRepo = require('../models/category'),
	constants = require('../fn/constants'),
	helper = require('../fn/helper');

var r = express.Router();

//DONE
r.get('/', function(req, res) {
	var vm = {};

	q.all([
		auctionRepo.loadAll_TopSoonestEnd(),
		auctionRepo.loadAll_TopHighestBidder(),
		auctionRepo.loadAll_TopHighestPrice()
	]).spread(function(top_end, top_bidder, top_price) {
		q.all([
			helper.parseInfoForListAuctionView(top_end, req.session.userID, 1000, constants.IMAGE_HEIGHT_HOME_PAGE),
			helper.parseInfoForListAuctionView(top_bidder, req.session.userID, 1000, constants.IMAGE_HEIGHT_HOME_PAGE),
			helper.parseInfoForListAuctionView(top_price, req.session.userID, 1000, constants.IMAGE_HEIGHT_HOME_PAGE)
		]).spread(function(top_end_cells, top_bidder_cells, top_price_cells) {
			if (top_end_cells.length > 0) top_end_cells[0].isActive = true;
			if (top_bidder_cells.length > 0) top_bidder_cells[0].isActive = true;
			if (top_price_cells.length > 0) top_price_cells[0].isActive = true;
			vm.top_end_cells = top_end_cells;
			vm.top_bidder_cells = top_bidder_cells;
			vm.top_price_cells = top_price_cells;
			return res.render('home/index', vm);
		}).fail(function(err) {
			console.log(err);
			return res.render('errors/database-error');
		});
	}).fail(function(err) {
		console.log(err);
		return res.render('errors/database-error');
	});
});

//DONE
r.post('/register', function(req, res) {

	var form = req.body;
	var data = {};
	data.success = false;

	//1 - check form format
	if (!(form.username && form.fullname && form.email && form.password)) {
		data.msg = 'Form data is not correct format.';
		return res.json(data);
	}

	//2 - check reCapcha
	if (req.body['g-recaptcha-response'] === undefined ||
		req.body['g-recaptcha-response'] === '' ||
		req.body['g-recaptcha-response'] === null) {
		return res.json({
			success: false,
			msg: "Please select Capcha!"
		});
	}
	var secretKey = "6LcsFSQUAAAAAOTjhqjvBS975xjNYLttuvI6o2yt";
	// url to send request
	var verificationUrl = "https://www.google.com/recaptcha/api/siteverify?secret=" +
		secretKey + "&response=" + req.body['g-recaptcha-response'] +
		"&remoteip=" + req.connection.remoteAddress;
	// send requesr to the URL, Google will respond with success or error scenario.
	request(verificationUrl, function(error, response, body) {
		body = JSON.parse(body);
		// Success will be true or false depending upon captcha validation.
		if (body.success !== undefined && !body.success) {
			return res.json({
				success: false,
				msg: "Failed captcha verification!"
			});
		}
		console.log("Check ok!" + body);
		//3 - create account
		form.fullname = helper.formatString(form.fullname);
		accountRepo.createAccount(form)
			.then(function() {
				req.session.userID = req.body.username;
				req.session.fullname = req.body.fullname;
				req.session.level = constants.ACCOUNT_TYPE_USER;

				data.success = true;
				return res.json(data);
			})
			.fail(function(error) {
				if (error.code == 'ER_DUP_ENTRY') {
					data.success = false;
					if (error.toString().search('PRIMARY') >= 0)
						data.msg = 'Username "' + req.body.username +
						'" is taken. Try another!';
					else data.msg = 'Email "' + req.body.email + '" has been used! Try another!';
				} else data.msg = "Error happened!";

				return res.json(data);
			});
	});
});
//DONE
r.post('/login', function(req, res) {
	var form = req.body;
	var data = {};
	data.success = false;

	//1 - check form format
	if (!(form.username && form.password)) {
		data.msg = 'Form data is not correct format.';
		return res.json(data);
	}

	//2 - check account login info
	accountRepo.checkAccountLoginInfo(form)
		.then(function(result) {
			if (result.success) {
				req.session.userID = result.userID;
				req.session.fullname = result.fullname;
				req.session.level = result.level;
			}
			return res.json(result);
		})
		.fail(function(error) {
			return res.json({
				success: false,
				msg: "Database error!"
			});
		});
});
//DONE
r.post('/logout', function(req, res) {
	var data = {};
	data.success = false;

	req.session.destroy(function(err) {
		if (err) {
			data.msg = "Can't log out. Error happened!";
			return res.json(data);
		} else {
			data.success = true;
			return res.json(data);
		}
	});
});

//DONE
// AUCTION SEARCH
r.get('/search', function(req, res) {
	var vm = {};
	vm.onlyStringOrCategorySearch = true;
	vm.hasQueryString = true;
	vm.noCategory = true;

	var q_str = req.query.q;
	var q_order = req.query.order;
	vm.query_string = querystring.escape(q_str).replace(/%20/g, "+");
	vm.order_string = querystring.escape(q_order);

	var words = helper.convertStringToWords(q_str);
	if (q_str.trim() == "") {
		return res.redirect('/');
	}
	vm.result_name = helper.formatString(q_str);

	if (words.length <= 0) {
		vm.result_count = 0;
		vm.isEmpty = true;
		return res.render('auction/list-auction', vm);
	}

	var temp = parseInt(req.query.page);
	var current_page = (!temp || temp <= 0) ? 1 : parseInt(req.query.page);

	var load_promise,
		promises = [];


	promises.push(auctionRepo.countAll_TitleContainsWords(words));

	switch (q_order) {
		case constants.ORDER_END_ASC:
			vm.order = "Time end: soon to late";
			load_promise = auctionRepo.loadLimit_TitleContainsWords_EndDateASC(words,
				(current_page - 1) * constants.NUM_AUCTION_PER_PAGE, constants.NUM_AUCTION_PER_PAGE);
			break;
		case constants.ORDER_END_DESC:
			vm.order = "Time end: late to soon";
			load_promise = auctionRepo.loadLimit_TitleContainsWords_EndDateDESC(words,
				(current_page - 1) * constants.NUM_AUCTION_PER_PAGE, constants.NUM_AUCTION_PER_PAGE);
			break;
		case constants.ORDER_PRICE_ASC:
			vm.order = "Curren price: low to high";
			load_promise = auctionRepo.loadLimit_TitleContainsWords_PriceASC(words,
				(current_page - 1) * constants.NUM_AUCTION_PER_PAGE, constants.NUM_AUCTION_PER_PAGE);
			break;
		case constants.ORDER_PRICE_DESC:
			vm.order = "Curren price: high to low";
			load_promise = auctionRepo.loadLimit_TitleContainsWords_PriceDESC(words,
				(current_page - 1) * constants.NUM_AUCTION_PER_PAGE, constants.NUM_AUCTION_PER_PAGE);
			break;
		default:
			vm.order_default = true;
			load_promise = auctionRepo.loadLimit_TitleContainsWords(words,
				(current_page - 1) * constants.NUM_AUCTION_PER_PAGE, constants.NUM_AUCTION_PER_PAGE);
	}

	q.all(promises).spread(function(totalRec) {

		var num_page = Math.ceil(totalRec / constants.NUM_AUCTION_PER_PAGE);

		vm.result_count = totalRec;
		if (num_page <= 1) vm.hidePageNav = true;
		if (totalRec <= 0 || current_page > num_page) {
			vm.isEmpty = true;
			return res.render('auction/list-auction', vm);
		} else {
			vm.pages = [];
			for (var i = 1; i <= num_page; i++) {
				vm.pages.push({
					num: i,
					isActive: false
				});
			}
			vm.pages[current_page - 1].isActive = true;


			load_promise.then(function(rows) {
				if (rows.length <= 0) vm.isEmpty = true;

				helper.parseInfoForListAuctionView(rows, req.session.userID, 1000, constants.IMAGE_HEIGHT_AUCTION_CELL).then(function(cells) {
					vm.auctions = cells;
					console.log(cells);
					return res.render('auction/list-auction', vm);
				}).fail(function(err) {
					console.log(err);
					return res.render('errors/database-error');
				});
			}).fail(function(err) {
				console.log(err);
				return res.render('errors/database-error');
			});
		}

	}).fail(function(err) {
		console.log(err);
		return res.render('errors/database-error');
	});
});

module.exports = r;