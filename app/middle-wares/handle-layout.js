var q = require('q'),
	categoryRepo = require('../models/category'),
	constants = require('../fn/constants');

module.exports = function(req, res, next) {

	res.locals.layoutVM = {};
	//navbar user info
	if (req.session.userID) {
		res.locals.layoutVM.logged_in = true;
		res.locals.layoutVM.fullname = req.session.fullname;
		if (req.session.level == constants.ACCOUNT_TYPE_ADMIN) {
			res.locals.layoutVM.isAdmin = true;
		}
	}

	//categories info
	q.all([
		categoryRepo.loadAll()
	]).spread(function(cRows) {
		res.locals.layoutVM.categories = cRows;
		next();
	}).catch(function(err) {
		console.log(err);
		return res.render("errors/database-error")
	});
}