function counter() {
  var now_date = new Date();

  $('.item').each(function() {
    var str_end_date = $(this).find('.end-date-string').text(),
      end_t = str_end_date.split(/[- :]/),
      end_date = new Date(Date.UTC(end_t[0], end_t[1] - 1, end_t[2], end_t[3], end_t[4], end_t[5]));

    if (now_date > end_date) {
      $(this).find('.text-timer').toggle();
      $(this).find('.text-over').toggle();
      $(this).find('.caption').css('background-color', 'black', 'important');
      return;
    }

    var diff = new Date(end_date - now_date);

    var days = Math.floor(diff / (86400000));
    var hrs = Math.floor((diff - (days * 86400000)) / (3600000));
    var mins = Math.floor((diff - (days * 86400000 + hrs * 3600000)) / (60000));
    var secs = Math.floor((diff - (days * 86400000 + hrs * 3600000 + mins * 60000)) / (1000));

    var days_str = Math.floor(days) + "";
    var hrs_str = Math.floor(hrs / 10) + "" + (hrs % 10);
    var mins_str = Math.floor(mins / 10) + "" + (mins % 10);
    var secs_str = Math.floor(secs / 10) + "" + (secs % 10);

    $($(this).find('.t-days')[0]).text(days_str);
    $($(this).find('.t-hrs')[0]).text(hrs_str);
    $($(this).find('.t-mins')[0]).text(mins_str);
    $($(this).find('.t-secs')[0]).text(secs_str);

  });
}

function hide_textover_timer() {
  $('.text-over').toggle();
}

$(document).ready(function() {
  hide_textover_timer();
  setInterval('counter()', 1000);
});