$(document).ready(function() {
	var id = $("#admin-panel-wrapper").find("a.active").prop("id");
	var content = $("#" + id + "-content");
	console.log(content);
	content.show();

	$(".date-string-span").each(function() {
		var date_string = $(this).text(),
			date_t = date_string.split(/[- :]/);
		date = new Date(Date.UTC(date_t[0], date_t[1] - 1, date_t[2],
			date_t[3], date_t[4], date_t[5]));
		$(this).parent().find('.date-span').text(date);
	});

	jQuery.validator.addMethod("noEmpty", function(value, element) {
		return value.trim();
	}, "Please not empty!");

	//Validate forms
	$("#form-update-password").validate({
		rules: {
			current_password: {
				required: true,
				minlength: 6,
			},
			new_password: {
				required: true,
				minlength: 6,
			},
			confirm_new_password: {
				equalTo: "#new_password",
			}
		},

		messages: {
			confirm_new_password: {
				equalTo: "Must same new password!"
			}

		}
	});
	$("#form-update-basic-info").validate({
		rules: {
			phone: {
				minlength: 9,
				maxlength: 12,
				number: true
			},
			fullname: {
				required: true,
				noEmpty: true
			}
		},

		messages: {
			phone: {
				minlength: "Phone number isn't right format! (9 - 12 numbers)",
				maxlength: "Phone number isn't right format! (9 - 12 numbers)",
				number: "Phone number isn't right format! (9 - 12 numbers)",
			}

		}
	});
});

$("#btn-update-basic-info").click(function() {
	var form = $("#form-update-basic-info");
	if (!form.valid()) return;
	$.post(form.attr('action'), form.serialize(), function(data) {

		if (data.needLogin) {
			$("#alert-login-Modal").modal();
			return;
		}

		var rltModal = $("#dashboard-result-Modal");
		if (data.success == true) {
			rltModal.find(".modal-title").text("Congratulation!");
			rltModal.find(".modal-body").html("<p>You update basic info success!<p>");
			rltModal.modal();

		} else {
			rltModal.find(".modal-title").text("Sorry!");
			rltModal.find(".modal-body").html(data.msg);
			rltModal.modal();
		}
	}, 'json');

	return;
});
$("#btn-update-email").click(function() {
	var form = $("#form-update-email");
	if (!form.valid()) return;
	$.post(form.attr('action'), form.serialize(), function(data) {

		if (data.needLogin) {
			$("#alert-login-Modal").modal();
			return;
		}

		var rltModal = $("#dashboard-result-Modal");
		if (data.success == true) {
			rltModal.find(".modal-title").text("Congratulation!");
			rltModal.find(".modal-body").html("<p>You update email success!<p>");
			rltModal.modal();

		} else {
			rltModal.find(".modal-title").text("Sorry!");
			rltModal.find(".modal-body").html(data.msg);
			rltModal.modal();
		}
	}, 'json');

	return;
});
$("#btn-update-password").click(function() {
	var form = $("#form-update-password");
	if (!form.valid()) return;
	$.post(form.attr('action'), form.serialize(), function(data) {

		$("#changePasswordModal").modal('hide');
		if (data.needLogin) {
			$("#alert-login-Modal").modal();
			return;
		}

		var rltModal = $("#dashboard-result-Modal");
		if (data.success == true) {
			rltModal.find(".modal-title").text("Congratulation!");
			rltModal.find(".modal-body").html("<p>You update password success!<p>");
			rltModal.modal();

		} else {
			rltModal.find(".modal-title").text("Sorry!");
			rltModal.find(".modal-body").html(data.msg);
			rltModal.modal();
		}
	}, 'json');

	return;
});
$("#dashboard-result-Modal").on('hidden.bs.modal', function() {
	location.reload();
});

$('.removeCategory').on('click', function() {
	var id = $(this).data('id');
	$('#txtDelCatId').val(id);
	$('#frmDeleteCategory').submit();
});

$('.removeAccount').on('click', function() {
	var id = $(this).data('id');
	$('#txtDelAccId').val(id);
	$('#frmDeleteAcc').submit();
});

$('.acceptRequest').on('click', function() {
	var accID = $(this).data('id');
	$('#txtAcceptRequest').val(accID);
	$('#frmAcceptRequest').submit();
});

$('.rejectRequest').on('click', function() {
	var accID = $(this).data('id');
	$('#txtRejectRequest').val(accID);
	$('#frmRejectRequest').submit();
});

$('.resetPassword').on('click', function() {
	var accID = $(this).data('id');
	$('#txtAccountId').val(accID);
	$('#frmResetPassword').submit();
});