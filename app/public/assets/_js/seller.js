$(document).ready(function() {
	$(".date-string-span").each(function() {
		var date_string = $(this).text(),
			date_t = date_string.split(/[- :]/);
		date = new Date(Date.UTC(date_t[0], date_t[1] - 1, date_t[2],
			date_t[3], date_t[4], date_t[5]));
		$(this).parent().find('.date-span').text(date);
	});
});