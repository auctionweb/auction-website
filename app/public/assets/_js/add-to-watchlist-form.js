$(".link-add-to-watchlist").click(function() {
	console.log("link-add-to-watchlist CLICKED");
	var form = $("#add-to-watchlist-form");
	$.post($(this).data("action-href"), form.serialize(), function(data) {

		if (data.success == true) {
			var span = $('<span class="label label-danger"><span class="glyphicon glyphicon-heart"></span>(Followed)</span>')
			$(this).parent().append(span);
			$(this).remove();
		} else if (data.needLogin) {
			alert("You need login to do this action!");
		} else {
			alert(data.msg);
		}
	}, 'json');
});