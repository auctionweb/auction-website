$(document).ready(function() {
	//valdator config
	$.validator.setDefaults({
		highlight: function(element) {
			$(element).closest('.form-group').addClass('has-error');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-error');
		},
		errorElement: 'span',
		errorClass: 'help-block',
		errorPlacement: function(error, element) {
			if (element.parent('.input-group').length) {
				error.insertAfter(element.parent());
			} else {
				error.insertAfter(element);
			}
		}
	});

	//custom validate method
	jQuery.validator.addMethod("usernamePattern", function(value, element) {
		var patt = /^[a-zA-Z0-9_-]*$/;
		return patt.test(value);
	}, "Please use only letters (a-z), numbers and underscore");
	jQuery.validator.addMethod("noEmpty", function(value, element) {
		return value.trim();
	}, "Please not empty!");

	//validate form
	$("#form-register").validate({
		rules: {
			username: {
				required: true,
				usernamePattern: true,
				minlength: 3,
				maxlength: 30,
			},
			fullname: {
				required: true,
				noEmpty: true,
				maxlength: 50,
			},
			email: {
				required: true,
				email: true,
				maxlength: 100,
			},
			password: {
				required: true,
				minlength: 6,
				maxlength: 50,
			},
			confirm_password: {
				equalTo: "#password",
			},

		},
		messages: {}
	});
	$("#form-login").validate({
		error: function(label) {
			$(this).addClass("error");
		},
		rules: {
			username: {
				required: true,
				usernamePattern: true,
				minlength: 3,
				maxlength: 30,
			},
			password: {
				required: true,
				minlength: 6,
				maxlength: 50,
			},

		},
		messages: {}
	});

	//error modals
	$("#logout_error_Modal").modal();
});

$("#form-register").submit(function() {
	if ($(this).valid()) {
		$.post($(this).attr('action'), $(this).serialize(), function(data) {
			if (data.success == true) {
				$("#registerModal").modal('hide');
				$("#register-success-Modal").modal();

			} else {
				$("#registerErrorMsg").html(data.msg);
				grecaptcha.reset();
			}
		}, 'json');
	}
	return false;
});
$("#form-login").submit(function() {
	if ($(this).valid()) {
		$.post($(this).attr('action'), $(this).serialize(), function(data) {
			console.log(data.success + data.msg);
			if (data.success == true) {
				$("#loginModal").modal('hide');
				location.reload();

			} else {
				$("#loginErrorMsg").html(data.msg);
			}
		}, 'json');
	}
	return false;
});
$("#link-logout").click(function() {
	var form = $("#form-logout");
	$.post(form.attr('action'), form.serialize(), function(data) {
		if (data.success == true) {
			location.reload();

		} else {
			alert(data.msg);
		}
	}, 'json');
});
$('#register-success-Modal').on('hidden.bs.modal', function() {
	location.reload();
})