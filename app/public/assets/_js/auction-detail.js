var start_date, end_date;

function setDates() {
  var timer = $('.auction-time');

  var str_start_date = $(timer).find('.start-date-string').text(),
    start_t = str_start_date.split(/[- :]/);

  start_date = new Date(Date.UTC(start_t[0], start_t[1] - 1, start_t[2], start_t[3], start_t[4], start_t[5]));
  var str_end_date = $(timer).find('.end-date-string').text(),
    end_t = str_end_date.split(/[- :]/);
  end_date = new Date(Date.UTC(end_t[0], end_t[1] - 1, end_t[2], end_t[3], end_t[4], end_t[5]));

  $(timer).find('.start-date-text').text(start_date)
  $(timer).find('.end-date-text').text(end_date)
}

function counter() {
  var now = new Date();
  var timer = $('.auction-time');

  var now_date = new Date();

  if (now_date > end_date) {
    $(timer).find('.text-timer').toggle();
    $(timer).find('.text-over').toggle();
    $(timer).css("background-color", "black");
    $('#btn-buy').prop('disabled', true);
    $('#btn-bid').prop('disabled', true);
    return;
  }

  var diff = new Date(end_date - now_date);

  var days = Math.floor(diff / (86400000));
  var hrs = Math.floor((diff - (days * 86400000)) / (3600000));
  var mins = Math.floor((diff - (days * 86400000 + hrs * 3600000)) / (60000));
  var secs = Math.floor((diff - (days * 86400000 + hrs * 3600000 + mins * 60000)) / (1000));

  var days_str = Math.floor(days) + "";
  var hrs_str = Math.floor(hrs / 10) + "" + (hrs % 10);
  var mins_str = Math.floor(mins / 10) + "" + (mins % 10);
  var secs_str = Math.floor(secs / 10) + "" + (secs % 10);

  $($(timer).find('.t-days')[0]).text(days_str);
  $($(timer).find('.t-hrs')[0]).text(hrs_str);
  $($(timer).find('.t-mins')[0]).text(mins_str);
  $($(timer).find('.t-secs')[0]).text(secs_str);
}

function hide_textover_timer() {
  $('.text-over').toggle();
}

function decodeHtml(html) {
  return $("<textarea/>").html(html).text();
}

$(document).ready(function() {
  hide_textover_timer();
  setDates();
  setInterval('counter()', 1000);

  //description decode and display
  var desc_text = $("#desc_container").text();
  $("#desc_container").html(decodeHtml(desc_text));

});

//add description
$("#add-info-submit").click(function() {
  var editorContent = tinymce.get('add_content').getContent();
  if (editorContent == '') {
    alert("Content is empty!");
    return;
  }
  tinyMCE.triggerSave();
  var form = $("#add-info-form");
  $.post(form.attr('action'), form.serialize(), function(data) {
    $("#add-info-Modal").modal('hide');
    if (data.needLogin) {
      $("#alert-login-Modal").modal();
      return;
    }

    if (data.success == true) {
      $("#add-info-success-Modal").modal();

    } else {
      $("#add-info-err-msg").html(data.msg);
    }
  }, 'json');

  return;
});
$("#add-info-success-Modal").on('hidden.bs.modal', function() {
  location.reload();
});

//transaction: buy now
$("#btn-buy").click(function() {
  var form = $("#buy-form");
  $.post(form.attr('action'), form.serialize(), function(data) {
    if (data.needLogin) {
      $("#alert-login-Modal").modal();
      return;
    }

    var rltModal = $("#transaction-result-Modal");
    if (data.success == true) {

      rltModal.find(".modal-title").text("Congratulation!");
      rltModal.find(".modal-body").html("<p>You bought success! This auction end!<p>");
      rltModal.modal();

    } else {
      rltModal.find(".modal-title").text("Sorry!");
      rltModal.find(".modal-body").html(data.msg);
      rltModal.modal();
    }
  }, 'json');

  return;
});

//transaction: bid
$("#btn-bid").click(function() {
  var form = $("#bid-form");
  $.post(form.attr('action'), form.serialize(), function(data) {
    console.log(data);
    $("#bid-Modal").modal('hide');
    if (data.needLogin) {
      $("#alert-login-Modal").modal();
      return;
    }

    var rltModal = $("#transaction-result-Modal");
    if (data.success == true) {
      rltModal.find(".modal-title").text("Congratulation!");
      rltModal.find(".modal-body").html("<p>You make bid success! You are highest bidder now!<p>");
      rltModal.modal();

    } else {
      rltModal.find(".modal-title").text("Sorry!");
      rltModal.find(".modal-body").html(data.msg);
      rltModal.modal();
    }
  }, 'json');

  return;
});

$("#transaction-result-Modal").on('hidden.bs.modal', function() {
  location.reload();
});