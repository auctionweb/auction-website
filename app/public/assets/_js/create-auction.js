var count = 0;
const MAX = 3;

$(document).ready(function() {
    var div = createImageInputFormGroup();
    $('.image-input-list').append(div);
    $('#auc_category').val($("#auc_category option:first").val());

    var date = new Date(); //local time
    var start = new Date(date.getTime() - (date.getTimezoneOffset() * 60000)); //zulu time
    $('input[type=datetime-local]').val(start.toJSON().slice(0, 16));
});

$('#form-create-auction').on('submit', function() {

    if ($('.image-input-list').find('.full').length <= 0) {
        alert('Choose at least 1 images !!!');
        return false;
    }

    var editorContent = tinymce.get('auc_desc').getContent();

    console.log(editorContent);
    if (editorContent == '') {
        alert("Description is empty!");
        return false;
    }

    var now = new Date($.now());
    // var start = new Date($("#StartTime").val());
    var end = new Date($("#date_end").val());
    console.log("NOW: " + now + "\nEND: " + end);

    // if (start < now) {
    //     alert('Start time must after "now" at least 1 minute !!!');
    //     return false;
    // }
    if (end <= now) {
        alert('End date must after "now" at least 1 minute !!!');
        return false;
    }
    $('#date_end_string').val(end.toISOString());
});

function createImageInputFormGroup() {
    var id = "image" + (count + 1);
    var div = $('<div class="form-group col-sm-4 col-xs-4 text-center input-image-wrappwer"></div>');
    var image_input = $('<input class="input-image-upload empty" type="file" name="' + id + '" id="' + id + '" accept="image/*">');
    var preview = $('<div><label for="' + id + '" class="preview"><img class="img-thumbnail" src="/assets/images/placeholder.png"></label></div>');
    div.append(image_input);
    div.append(preview);

    image_input.change(handleImageInputChange);

    return div;
}
var handleImageInputChange = function() {
    var div = $(this).parent();
    var image_preview = div.find('img');
    var image_input = div.find('input');

    if (image_input.prop('files')[0]) {
        // Use FileReader to get file
        var reader = new FileReader();

        //image has loaded --> preview
        reader.onload = function(e) {
            image_preview.attr('src', e.target.result);
            if (div.find('.full').length > 0) return;

            count++;
            image_input.removeClass('empty').addClass('full');
            var btn_remove = $('<button type="button" class="btn-remove">Remove</button>');
            div.append(btn_remove);

            btn_remove.click(function() {
                var parent = div.parent();
                div.remove();
                count--;

                parent.find('.input-image-wrappwer').each(function(index) {
                    var id = 'image' + (index + 1);
                    $(this).find('input').attr('name', id);
                    $(this).find('input').attr('id', id);
                    $(this).find('label').attr('for', id);
                });

                if (count == (MAX - 1)) {
                    var new_input_group = createImageInputFormGroup();
                    $('.image-input-list').append(new_input_group);
                }
            });

            if (count < MAX) {
                var new_input_group = createImageInputFormGroup();
                $('.image-input-list').append(new_input_group);
            }
        }

        // Load image
        reader.readAsDataURL($(this).prop('files')[0]);
    }
}