var mustache = require('mustache'),
	q = require('q');
var db = require('../fn/db');

exports.loadAll_ByAuctionID_OrderByDateDESC = function(auc_id) {
	var obj = {
		auction_id: auc_id
	};
	var sql = mustache.render("SELECT * FROM `Auction-BidLogger` " +
		"WHERE `auction-id` = '{{auction_id}}' " +
		"ORDER BY `date` DESC;",
		obj
	);
	return db.load(sql);
};
exports.loadAll_ByAuctionID_OrderByDateASC = function(auc_id) {
	var obj = {
		auction_id: auc_id
	};
	var sql = mustache.render("SELECT * FROM `Auction-BidLogger` " +
		"WHERE `auction-id` = '{{auction_id}}' " +
		"ORDER BY `date` ASC;",
		obj
	);
	return db.load(sql);
};
exports.loadAll_ByAuctionID_OrderByLogIDDESC = function(auc_id) {
	var obj = {
		auction_id: auc_id
	};
	var sql = mustache.render("SELECT * FROM `Auction-BidLogger` " +
		"WHERE `auction-id` = '{{auction_id}}' " +
		"ORDER BY `id` DESC;",
		obj
	);
	return db.load(sql);
};

exports.loadAll_ByAccountID = function(acc_id) {
	var obj = {
		account_id: acc_id
	};
	var sql = mustache.render("SELECT * FROM `Auction-BidLogger` WHERE `account-id` = '{{account_id}}';",
		obj
	);
	return db.load(sql);
};

exports.loadAll_ByAuctionIDAndAccountID = function(auc_id, acc_id) {
	var obj = {
		account_id: acc_id,
		auction_id: auc_id
	};
	var sql = mustache.render("SELECT * FROM `Auction-BidLogger` WHERE " +
		"`account-id` = '{{account_id}}' AND `auction-id` = '{{auction-id}}';",
		obj
	);
	return db.load(sql);
};

exports.insert = function(entity) {
	var sql = mustache.render(
		"INSERT INTO `Auction-BidLogger` (`auction-id`, `account-id`, `date`, `price`) VALUES " +
		"('{{auction-id}}', '{{account-id}}', '{{date}}', '{{price}}');",
		entity
	);

	return db.insert(sql);
}


exports.delete = function(id) {
	var sql = mustache.render(
		"DELETE FROM `Auction-BidLogger` WHERE `id` = '{{id}}';", {
			id: id
		}
	);

	return db.delete(sql);
}
exports.deleteAll_By_AuctionID_And_AccountID = function(auc_id, acc_id) {
	var sql = mustache.render(
		"DELETE FROM `Auction-BidLogger` WHERE `auction-id` = '{{auction-id}}' AND `account-id` = '{{account-id}}';", {
			'auction-id': auc_id,
			'account-id': acc_id
		}
	);

	return db.delete(sql);
}

exports.loadDetail = function(id) {
	var d = q.defer();
	var obj = {
		id: id
	};
	var sql = mustache.render(
		"SELECT * FROM `Auction-BidLogger` WHERE `id` = {{id}}",
		obj
	)



	db.load(sql).then(function(rows) {
		d.resolve(rows[0]);
	}).fail(function(err) {
		d.reject(err);
	});

	return d.promise;
};
exports.loadHighestDetail = function(auc_id) {
	var d = q.defer();

	var obj = {
		auction_id: auc_id
	};
	var sql = mustache.render("SELECT * FROM `Auction-BidLogger` " +
		"WHERE `auction-id` = '{{auction_id}}' " +
		"ORDER BY `id` DESC LIMIT 1;",
		obj
	);

	db.load(sql).then(function(rows) {
		d.resolve(rows[0]);
	}).fail(function(err) {
		d.reject(err);
	});

	return d.promise;
};