var mustache = require('mustache'),
	q = require('q');
var db = require('../fn/db'),
	constants = require('../fn/constants');

//______________BASIC_______________________
exports.loadAll = function() {
	var d = q.defer();

	var obj = {
		"cat-id": cat_id
	};

	var sql = "SELECT *, DATE_FORMAT(`start-date`,'%Y-%m-%d %h:%i:%s') AS `start-date-string`, " +
		"DATE_FORMAT(`end-date`,'%Y-%m-%d %h:%i:%s') AS `end-date-string` FROM `Auction` " +
		"WHERE `end-date` > UTC_TIME();";

	db.load(sql).then(function(rows) {
		d.resolve(rows);
	}).fail(function(err) {
		d.reject(err);
	});

	return d.promise;
};
exports.loadDetail = function(id) {
	var d = q.defer();

	var obj = {
		id: id
	};

	var sql = mustache.render(
		"SELECT *, DATE_FORMAT(`start-date`,'%Y-%m-%d %h:%i:%s') AS `start-date-string`, " +
		"DATE_FORMAT(`end-date`,'%Y-%m-%d %h:%i:%s') AS `end-date-string` FROM `Auction` WHERE `id` = '{{id}}';",
		obj
	);

	db.load(sql).then(function(rows) {
		d.resolve(rows[0]);
	}).fail(function(err) {
		d.reject(err);
	});

	return d.promise;
};
exports.insert = function(entity) {
	var sql = mustache.render(
		"INSERT INTO `Auction` (`seller-id`, `plus-minute`, `auto-support`, `title`, `description`, `start-date`, `end-date`, " +
		"`start-price`, `bid-increment`, `has-buyout`, `buyout-price`, `highest-id`, `cat-id`, `current-price`, `has-winner`, `MAX-PRICE`) " +
		"VALUES ('{{seller-id}}', '{{plus-minute}}', {{auto-support}}, '{{title}}', '{{description}}', '{{start-date}}', '{{end-date}}', '{{start-price}}', '{{bid-increment}}', {{has-buyout}}, " +
		"'{{buyout-price}}', '{{highest-id}}', '{{cat-id}}', '{{current-price}}', {{has-winner}}, '{{MAX-PRICE}}');",
		entity
	);

	return db.insert(sql);
};
exports.update = function(entity) {
	entity['start-date'] = entity['start-date'].toISOString().slice(0, 19).replace('T', ' ');
	entity['end-date'] = entity['end-date'].toISOString().slice(0, 19).replace('T', ' ');
	var sql = mustache.render(
		"UPDATE `Auction` SET `seller-id` = '{{seller-id}}', `plus-minute` = '{{plus-minute}}', " +
		"`auto-support` = {{auto-support}}, `title` = '{{title}}', `description` = '{{description}}', `start-date` = '{{start-date}}', " +
		"`end-date` = '{{end-date}}', `start-price` = '{{start-price}}', `bid-increment` = '{{bid-increment}}', " +
		"`has-buyout` = {{has-buyout}}, `buyout-price` = '{{buyout-price}}', `highest-id` = '{{highest-id}}', " +
		"`cat-id` = '{{cat-id}}', `current-price` = '{{current-price}}', `has-winner` = {{has-winner}}, `MAX-PRICE` = '{{MAX-PRICE}}' " +
		"WHERE `id` = '{{id}}';",
		entity
	);

	return db.update(sql);
};
exports.delete = function(entity) {
	var sql = mustache.render(
		"DELETE FROM `Auction` WHERE `id` = '{{id}}'",
		entity
	);

	return db.delete(sql);
};

//________________LOCK AND UPDATE_____________________
// exports.loadDetail_LockForUpdate = function(id) {
// 	var d = q.defer();

// 	var obj = {
// 		id: id
// 	};

// 	var sql = mustache.render("SELECT * FROM `Auction` where `id` = '{{id}}' AND `end-date` > UTC_TIME()  FOR UPDATE; ", obj);

// 	db.load(sql).then(function(rows) {
// 		d.resolve(rows[0]);
// 	}).fail(function(err) {
// 		d.reject(err);
// 	});

// 	return d.promise;
// };

//________________LOADALL_____________________
exports.countAll_ByCatID = function(cat_id) {
	var d = q.defer();

	var obj = {
		"cat-id": cat_id
	};

	var sql = mustache.render("SELECT COUNT(*) AS `count` FROM `Auction` " +
		"WHERE `cat-id` = '{{cat-id}}' AND `end-date` > UTC_TIME();", obj);

	db.load(sql).then(function(rows) {
		d.resolve(rows[0]["count"]);

	}).fail(function(error) {
		console.log(error);
		d.reject(error);
	});

	return d.promise;
};
exports.loadAll_ByCatID = function(cat_id) {
	var d = q.defer();

	var obj = {
		"cat-id": cat_id
	};

	var sql = mustache.render("SELECT *, DATE_FORMAT(`start-date`,'%Y-%m-%d %h:%i:%s') AS `start-date-string`, " +
		"DATE_FORMAT(`end-date`,'%Y-%m-%d %h:%i:%s') AS `end-date-string` FROM `Auction` " +
		"WHERE `cat-id` = '{{cat-id}}' AND `end-date` > UTC_TIME();", obj);

	db.load(sql).then(function(rows) {
		d.resolve(rows);
	}).fail(function(err) {
		d.reject(err);
	});

	return d.promise;
};
exports.loadLimit_ByCatID = function(cat_id, offset, num) {
	var d = q.defer();

	var obj = {
		"cat-id": cat_id,
		"offset": offset,
		"num": num
	};

	var sql = mustache.render("SELECT *, DATE_FORMAT(`start-date`,'%Y-%m-%d %h:%i:%s') AS `start-date-string`, " +
		"DATE_FORMAT(`end-date`,'%Y-%m-%d %h:%i:%s') AS `end-date-string` FROM `Auction` " +
		"WHERE `cat-id` = '{{cat-id}}' AND `end-date` > UTC_TIME() " +
		"LIMIT {{offset}}, {{num}};", obj);

	db.load(sql).then(function(rows) {
		d.resolve(rows);
	}).fail(function(err) {
		d.reject(err);
	});

	return d.promise;
};
exports.loadLimit_ByCatID_EndDateASC = function(cat_id, offset, num) {
	var d = q.defer();

	var obj = {
		"cat-id": cat_id,
		"offset": offset,
		"num": num
	};

	var sql = mustache.render("SELECT *, DATE_FORMAT(`start-date`,'%Y-%m-%d %h:%i:%s') AS `start-date-string`, " +
		"DATE_FORMAT(`end-date`,'%Y-%m-%d %h:%i:%s') AS `end-date-string` FROM `Auction` " +
		"WHERE `cat-id` = '{{cat-id}}' AND `end-date` > UTC_TIME() " +
		"ORDER BY `end-date` ASC " +
		"LIMIT {{offset}}, {{num}};", obj);

	db.load(sql).then(function(rows) {
		d.resolve(rows);
	}).fail(function(err) {
		d.reject(err);
	});

	return d.promise;
};
exports.loadLimit_ByCatID_EndDateDESC = function(cat_id, offset, num) {
	var d = q.defer();

	var obj = {
		"cat-id": cat_id,
		"offset": offset,
		"num": num
	};

	var sql = mustache.render("SELECT *, DATE_FORMAT(`start-date`,'%Y-%m-%d %h:%i:%s') AS `start-date-string`, " +
		"DATE_FORMAT(`end-date`,'%Y-%m-%d %h:%i:%s') AS `end-date-string` FROM `Auction` " +
		"WHERE `cat-id` = '{{cat-id}}' AND `end-date` > UTC_TIME() " +
		"ORDER BY `end-date` DESC " +
		"LIMIT {{offset}}, {{num}};", obj);

	db.load(sql).then(function(rows) {
		d.resolve(rows);
	}).fail(function(err) {
		d.reject(err);
	});

	return d.promise;
};
exports.loadLimit_ByCatID_PriceASC = function(cat_id, offset, num) {
	var d = q.defer();

	var obj = {
		"cat-id": cat_id,
		"offset": offset,
		"num": num
	};

	var sql = mustache.render("SELECT *, DATE_FORMAT(`start-date`,'%Y-%m-%d %h:%i:%s') AS `start-date-string`, " +
		"DATE_FORMAT(`end-date`,'%Y-%m-%d %h:%i:%s') AS `end-date-string` FROM `Auction` " +
		"WHERE `cat-id` = '{{cat-id}}' AND `end-date` > UTC_TIME() " +
		"ORDER BY `current-price` ASC " +
		"LIMIT {{offset}}, {{num}};", obj);

	db.load(sql).then(function(rows) {
		d.resolve(rows);
	}).fail(function(err) {
		d.reject(err);
	});

	return d.promise;
};
exports.loadLimit_ByCatID_PriceDESC = function(cat_id, offset, num) {
	var d = q.defer();

	var obj = {
		"cat-id": cat_id,
		"offset": offset,
		"num": num
	};

	var sql = mustache.render("SELECT *, DATE_FORMAT(`start-date`,'%Y-%m-%d %h:%i:%s') AS `start-date-string`, " +
		"DATE_FORMAT(`end-date`,'%Y-%m-%d %h:%i:%s') AS `end-date-string` FROM `Auction` " +
		"WHERE `cat-id` = '{{cat-id}}' AND `end-date` > UTC_TIME() " +
		"ORDER BY `current-price` DESC " +
		"LIMIT {{offset}}, {{num}};", obj);

	db.load(sql).then(function(rows) {
		d.resolve(rows);
	}).fail(function(err) {
		d.reject(err);
	});

	return d.promise;
};



exports.countAll_TitleContainsWords = function(words) {
	var d = q.defer();

	if (words.length <= 0) {
		d.resolve(null);
		return d.promise;
	}

	var sql = "SELECT COUNT(*) AS `count` FROM `Auction` WHERE ";
	for (var i = 0; i < words.length - 1; i++) {
		var temp = "`title` REGEXP '" + words[i] + "' AND ";
		sql += temp;
	}
	sql += "`title` REGEXP '" + words[i] + "' AND `end-date` > UTC_TIME();";

	db.load(sql).then(function(rows) {
		d.resolve(rows[0]["count"]);

	}).fail(function(error) {
		console.log(error);
		d.reject(error);
	});

	return d.promise;
};
exports.loadAll_TitleContainsWords = function(words) {
	var d = q.defer();

	if (words.length <= 0) {
		d.resolve(null);
		return d.promise;
	}

	var sql = "SELECT *, DATE_FORMAT(`start-date`,'%Y-%m-%d %h:%i:%s') AS `start-date-string`, " +
		"DATE_FORMAT(`end-date`,'%Y-%m-%d %h:%i:%s') AS `end-date-string` FROM `Auction` WHERE ";
	for (var i = 0; i < words.length - 1; i++) {
		var temp = "`title` REGEXP '" + words[i] + "' AND ";
		sql += temp;
	}
	sql += "`title` REGEXP '" + words[i] + "' AND `end-date` > UTC_TIME();";

	db.load(sql).then(function(rows) {
		d.resolve(rows);
	}).fail(function(err) {
		d.reject(err);
	});

	return d.promise;
};
exports.loadLimit_TitleContainsWords = function(words, offset, num) {
	var d = q.defer();

	if (words.length <= 0) {
		d.resolve(null);
		return d.promise;
	}

	var sql = "SELECT *, DATE_FORMAT(`start-date`,'%Y-%m-%d %h:%i:%s') AS `start-date-string`, " +
		"DATE_FORMAT(`end-date`,'%Y-%m-%d %h:%i:%s') AS `end-date-string` FROM `Auction` WHERE ";
	for (var i = 0; i < words.length - 1; i++) {
		var temp = "`title` REGEXP '" + words[i] + "' AND ";
		sql += temp;
	}
	sql += "`title` REGEXP '" + words[i] + "' AND `end-date` > UTC_TIME() ";
	sql += mustache.render("LIMIT {{offset}}, {{num}}", {
		offset: offset,
		num: num
	});

	db.load(sql).then(function(rows) {
		d.resolve(rows);
	}).fail(function(err) {
		d.reject(err);
	});

	return d.promise;
};
exports.loadLimit_TitleContainsWords_EndDateASC = function(words, offset, num) {
	var d = q.defer();

	if (words.length <= 0) {
		d.resolve(null);
		return d.promise;
	}

	var sql = "SELECT *, DATE_FORMAT(`start-date`,'%Y-%m-%d %h:%i:%s') AS `start-date-string`, " +
		"DATE_FORMAT(`end-date`,'%Y-%m-%d %h:%i:%s') AS `end-date-string` FROM `Auction` WHERE ";
	for (var i = 0; i < words.length - 1; i++) {
		var temp = "`title` REGEXP '" + words[i] + "' AND ";
		sql += temp;
	}
	sql += "`title` REGEXP '" + words[i] + "' AND `end-date` > UTC_TIME() ";
	sql += "ORDER BY `end-date` ASC ";
	sql += mustache.render("LIMIT {{offset}}, {{num}}", {
		offset: offset,
		num: num
	});

	db.load(sql).then(function(rows) {
		d.resolve(rows);
	}).fail(function(err) {
		d.reject(err);
	});

	return d.promise;
};
exports.loadLimit_TitleContainsWords_EndDateDESC = function(words, offset, num) {
	var d = q.defer();

	if (words.length <= 0) {
		d.resolve(null);
		return d.promise;
	}

	var sql = "SELECT *, DATE_FORMAT(`start-date`,'%Y-%m-%d %h:%i:%s') AS `start-date-string`, " +
		"DATE_FORMAT(`end-date`,'%Y-%m-%d %h:%i:%s') AS `end-date-string` FROM `Auction` WHERE ";
	for (var i = 0; i < words.length - 1; i++) {
		var temp = "`title` REGEXP '" + words[i] + "' AND ";
		sql += temp;
	}
	sql += "`title` REGEXP '" + words[i] + "' AND `end-date` > UTC_TIME() ";
	sql += "ORDER BY `end-date` DESC ";
	sql += mustache.render("LIMIT {{offset}}, {{num}}", {
		offset: offset,
		num: num
	});

	db.load(sql).then(function(rows) {
		d.resolve(rows);
	}).fail(function(err) {
		d.reject(err);
	});

	return d.promise;
};
exports.loadLimit_TitleContainsWords_PriceASC = function(words, offset, num) {
	var d = q.defer();

	if (words.length <= 0) {
		d.resolve(null);
		return d.promise;
	}

	var sql = "SELECT *, DATE_FORMAT(`start-date`,'%Y-%m-%d %h:%i:%s') AS `start-date-string`, " +
		"DATE_FORMAT(`end-date`,'%Y-%m-%d %h:%i:%s') AS `end-date-string` FROM `Auction` WHERE ";
	for (var i = 0; i < words.length - 1; i++) {
		var temp = "`title` REGEXP '" + words[i] + "' AND ";
		sql += temp;
	}
	sql += "`title` REGEXP '" + words[i] + "' AND `end-date` > UTC_TIME() ";
	sql += "ORDER BY `current-price` ASC ";
	sql += mustache.render("LIMIT {{offset}}, {{num}}", {
		offset: offset,
		num: num
	});

	db.load(sql).then(function(rows) {
		d.resolve(rows);
	}).fail(function(err) {
		d.reject(err);
	});

	return d.promise;
};
exports.loadLimit_TitleContainsWords_PriceDESC = function(words, offset, num) {
	var d = q.defer();

	if (words.length <= 0) {
		d.resolve(null);
		return d.promise;
	}

	var sql = "SELECT *, DATE_FORMAT(`start-date`,'%Y-%m-%d %h:%i:%s') AS `start-date-string`, " +
		"DATE_FORMAT(`end-date`,'%Y-%m-%d %h:%i:%s') AS `end-date-string` FROM `Auction` WHERE ";
	for (var i = 0; i < words.length - 1; i++) {
		var temp = "`title` REGEXP '" + words[i] + "' AND ";
		sql += temp;
	}
	sql += "`title` REGEXP '" + words[i] + "' AND `end-date` > UTC_TIME() ";
	sql += "ORDER BY `current-price` DESC ";
	sql += mustache.render("LIMIT {{offset}}, {{num}}", {
		offset: offset,
		num: num
	});

	db.load(sql).then(function(rows) {
		d.resolve(rows);
	}).fail(function(err) {
		d.reject(err);
	});

	return d.promise;
};

exports.countAll_TitleContainsWords_ByCatID = function(words, cat_id) {
	var d = q.defer();

	if (words.length <= 0) {
		d.resolve(null);
		return d.promise;
	}

	var sql = "SELECT COUNT(*) AS `count` FROM `Auction` WHERE ";
	for (var i = 0; i < words.length; i++) {
		var temp = "`title` REGEXP '" + words[i] + "' AND ";
		sql += temp;
	}
	sql += "`cat-id` = '" + cat_id + "' AND `end-date` > UTC_TIME();";

	db.load(sql).then(function(rows) {
		d.resolve(rows[0]["count"]);

	}).fail(function(error) {
		console.log(error);
		d.reject(error);
	});

	return d.promise;
};
exports.loadAll_TitleContainsWords_ByCatID = function(words, cat_id) {
	var d = q.defer();

	if (words.length <= 0) {
		d.resolve(null);
		return d.promise;
	}

	var sql = "SELECT *, DATE_FORMAT(`start-date`,'%Y-%m-%d %h:%i:%s') AS `start-date-string`, " +
		"DATE_FORMAT(`end-date`,'%Y-%m-%d %h:%i:%s') AS `end-date-string` FROM `Auction` WHERE ";
	for (var i = 0; i < words.length; i++) {
		var temp = "`title` REGEXP '" + words[i] + "' AND ";
		sql += temp;
	}
	sql += "`cat-id` = '" + cat_id + "' AND `end-date` > UTC_TIME();";

	db.load(sql).then(function(rows) {
		d.resolve(rows);
	}).fail(function(err) {
		d.reject(err);
	});

	return d.promise;
};
exports.loadLimit_TitleContainsWords_ByCatID = function(words, cat_id, offset, num) {
	var d = q.defer();

	if (words.length <= 0) {
		d.resolve(null);
		return d.promise;
	}

	var sql = "SELECT *, DATE_FORMAT(`start-date`,'%Y-%m-%d %h:%i:%s') AS `start-date-string`, " +
		"DATE_FORMAT(`end-date`,'%Y-%m-%d %h:%i:%s') AS `end-date-string` FROM `Auction` WHERE ";
	for (var i = 0; i < words.length; i++) {
		var temp = "`title` REGEXP '" + words[i] + "' AND ";
		sql += temp;
	}
	sql += "`cat-id` = '" + cat_id + "' AND `end-date` > UTC_TIME() ";
	sql += mustache.render("LIMIT {{offset}}, {{num}}", {
		offset: offset,
		num: num
	});

	db.load(sql).then(function(rows) {
		d.resolve(rows);
	}).fail(function(err) {
		d.reject(err);
	});

	return d.promise;
};
exports.loadLimit_TitleContainsWords_ByCatID_EndDateASC = function(words, cat_id, offset, num) {
	var d = q.defer();

	if (words.length <= 0) {
		d.resolve(null);
		return d.promise;
	}

	var sql = "SELECT *, DATE_FORMAT(`start-date`,'%Y-%m-%d %h:%i:%s') AS `start-date-string`, " +
		"DATE_FORMAT(`end-date`,'%Y-%m-%d %h:%i:%s') AS `end-date-string` FROM `Auction` WHERE ";
	for (var i = 0; i < words.length; i++) {
		var temp = "`title` REGEXP '" + words[i] + "' AND ";
		sql += temp;
	}
	sql += "`cat-id` = '" + cat_id + "' AND `end-date` > UTC_TIME() ";
	sql += "ORDER BY `end-date` ASC ";
	sql += mustache.render("LIMIT {{offset}}, {{num}}", {
		offset: offset,
		num: num
	});

	db.load(sql).then(function(rows) {
		d.resolve(rows);
	}).fail(function(err) {
		d.reject(err);
	});

	return d.promise;
};
exports.loadLimit_TitleContainsWords_ByCatID_EndDateDESC = function(words, cat_id, offset, num) {
	var d = q.defer();

	if (words.length <= 0) {
		d.resolve(null);
		return d.promise;
	}

	var sql = "SELECT *, DATE_FORMAT(`start-date`,'%Y-%m-%d %h:%i:%s') AS `start-date-string`, " +
		"DATE_FORMAT(`end-date`,'%Y-%m-%d %h:%i:%s') AS `end-date-string` FROM `Auction` WHERE ";
	for (var i = 0; i < words.length; i++) {
		var temp = "`title` REGEXP '" + words[i] + "' AND ";
		sql += temp;
	}
	sql += "`cat-id` = '" + cat_id + "' AND `end-date` > UTC_TIME() ";
	sql += "ORDER BY `end-date` DESC ";
	sql += mustache.render("LIMIT {{offset}}, {{num}}", {
		offset: offset,
		num: num
	});

	db.load(sql).then(function(rows) {
		d.resolve(rows);
	}).fail(function(err) {
		d.reject(err);
	});

	return d.promise;
};
exports.loadLimit_TitleContainsWords_ByCatID_PriceASC = function(words, cat_id, offset, num) {
	var d = q.defer();

	if (words.length <= 0) {
		d.resolve(null);
		return d.promise;
	}

	var sql = "SELECT *, DATE_FORMAT(`start-date`,'%Y-%m-%d %h:%i:%s') AS `start-date-string`, " +
		"DATE_FORMAT(`end-date`,'%Y-%m-%d %h:%i:%s') AS `end-date-string` FROM `Auction` WHERE ";
	for (var i = 0; i < words.length; i++) {
		var temp = "`title` REGEXP '" + words[i] + "' AND ";
		sql += temp;
	}
	sql += "`cat-id` = '" + cat_id + "' AND `end-date` > UTC_TIME() ";
	sql += "ORDER BY `current-price` ASC ";
	sql += mustache.render("LIMIT {{offset}}, {{num}}", {
		offset: offset,
		num: num
	});

	db.load(sql).then(function(rows) {
		d.resolve(rows);
	}).fail(function(err) {
		d.reject(err);
	});

	return d.promise;
};
exports.loadLimit_TitleContainsWords_ByCatID_PriceDESC = function(words, cat_id, offset, num) {
	var d = q.defer();

	if (words.length <= 0) {
		d.resolve(null);
		return d.promise;
	}

	var sql = "SELECT *, DATE_FORMAT(`start-date`,'%Y-%m-%d %h:%i:%s') AS `start-date-string`, " +
		"DATE_FORMAT(`end-date`,'%Y-%m-%d %h:%i:%s') AS `end-date-string` FROM `Auction` WHERE ";
	for (var i = 0; i < words.length; i++) {
		var temp = "`title` REGEXP '" + words[i] + "' AND ";
		sql += temp;
	}
	sql += "`cat-id` = '" + cat_id + "' AND `end-date` > UTC_TIME() ";
	sql += "ORDER BY `current-price` DESC ";
	sql += mustache.render("LIMIT {{offset}}, {{num}}", {
		offset: offset,
		num: num
	});

	db.load(sql).then(function(rows) {
		d.resolve(rows);
	}).fail(function(err) {
		d.reject(err);
	});

	return d.promise;
};

exports.loadAll_WatchList = function(acc_id) {
	var obj = {
		'account-id': acc_id
	};

	var sql = mustache.render(
		"SELECT A.*, DATE_FORMAT(A.`start-date`,'%Y-%m-%d %h:%i:%s') AS `start-date-string`, " +
		"DATE_FORMAT(A.`end-date`,'%Y-%m-%d %h:%i:%s') AS `end-date-string` " +
		"FROM `Auction` AS A, `Auction-Watchlist` AS W " +
		"WHERE W.`account-id` = '{{account-id}}' AND  W.`auction-id` = A.`id`;",
		obj
	);

	return db.load(sql);
};
exports.loadAll_WinList = function(acc_id) {
	var obj = {
		'highest-id': acc_id
	};

	var sql = mustache.render(
		"SELECT *, DATE_FORMAT(`start-date`,'%Y-%m-%d %h:%i:%s') AS `start-date-string`, " +
		"DATE_FORMAT(`end-date`,'%Y-%m-%d %h:%i:%s') AS `end-date-string` " +
		"FROM `Auction` WHERE `has-winner` = true AND `highest-id` = '{{highest-id}}';",
		obj
	);

	return db.load(sql);
};
exports.loadAll_SoldList = function(acc_id) {
	var obj = {
		'seller-id': acc_id
	};

	var sql = mustache.render(
		"SELECT *, DATE_FORMAT(`start-date`,'%Y-%m-%d %h:%i:%s') AS `start-date-string`, " +
		"DATE_FORMAT(`end-date`,'%Y-%m-%d %h:%i:%s') AS `end-date-string` " +
		"FROM `Auction` WHERE `has-winner` = true AND `seller-id` = '{{seller-id}}';",
		obj
	);

	return db.load(sql);
};
exports.loadAll_SellingList = function(acc_id) {
	var obj = {
		'seller-id': acc_id
	};

	var sql = mustache.render(
		"SELECT *, DATE_FORMAT(`start-date`,'%Y-%m-%d %h:%i:%s') AS `start-date-string`, " +
		"DATE_FORMAT(`end-date`,'%Y-%m-%d %h:%i:%s') AS `end-date-string` " +
		"FROM `Auction` WHERE `end-date` > UTC_TIME() AND `seller-id` = '{{seller-id}}';",
		obj
	);

	return db.load(sql);
};
exports.loadAll_BiddingList = function(acc_id) {
	var obj = {
		'account-id': acc_id
	};

	var sql = mustache.render(
		"SELECT A.*, DATE_FORMAT(A.`start-date`,'%Y-%m-%d %h:%i:%s') AS `start-date-string`, " +
		"DATE_FORMAT(A.`end-date`,'%Y-%m-%d %h:%i:%s') AS `end-date-string` " +
		"FROM `Auction` as A, `Auction-BidLogger` AS B " +
		"WHERE B.`auction-id` = A.`id`  AND B.`account-id` = '{{account-id}}' AND A.`end-date` > UTC_TIME() " +
		"GROUP BY B.`auction-id`, B.`account-id`;",
		obj
	);
	return db.load(sql);
};

//________________LOAD TOP FOR HOME PAGE_____________________
exports.loadAll_TopSoonestEnd = function() {
	var obj = {
		topNumber: constants.TOP_AUCTION_NUMBER
	};

	var sql = mustache.render(
		"SELECT *, DATE_FORMAT(`start-date`,'%Y-%m-%d %h:%i:%s') AS `start-date-string`, " +
		"DATE_FORMAT(`end-date`,'%Y-%m-%d %h:%i:%s') AS `end-date-string` " +
		"FROM `Auction` WHERE `end-date` > UTC_TIME() ORDER BY `end-date` ASC LIMIT {{topNumber}};",
		obj
	);

	return db.load(sql);
};
exports.loadAll_TopHighestBidder = function() {
	var obj = {
		topNumber: constants.TOP_AUCTION_NUMBER
	};

	var sql = mustache.render(
		"SELECT *, DATE_FORMAT(`start-date`,'%Y-%m-%d %h:%i:%s') AS `start-date-string`, " +
		"DATE_FORMAT(`end-date`,'%Y-%m-%d %h:%i:%s') AS `end-date-string` FROM `Auction` " +
		"INNER JOIN (SELECT COUNT(*) as `bids`, `auction-id` FROM `Auction-BidLogger` GROUP BY `auction-id`) as `A` " +
		"ON `A`.`auction-id` = `Auction`.`id` " +
		"WHERE `Auction`.`end-date` > UTC_TIME() AND `Auction`.`has-winner` = 0 ORDER BY A.`bids` DESC LIMIT {{topNumber}};",
		obj
	);
	return db.load(sql);
};
exports.loadAll_TopHighestPrice = function() {
	var obj = {
		topNumber: constants.TOP_AUCTION_NUMBER
	};

	var sql = mustache.render(
		"SELECT *, DATE_FORMAT(`start-date`,'%Y-%m-%d %h:%i:%s') AS `start-date-string`, " +
		"DATE_FORMAT(`end-date`,'%Y-%m-%d %h:%i:%s') AS `end-date-string` " +
		"FROM `Auction` WHERE `end-date` > UTC_TIME() ORDER BY `current-price` DESC LIMIT {{topNumber}};",
		obj
	);

	return db.load(sql);
};

//________________OTHER_____________________
exports.updateDescription = function(auc_id, add_info) {
	var obj = {
		'id': auc_id,
		'add_info': add_info
	};

	var sql = mustache.render("UPDATE `Auction` SET `description` = " +
		"CONCAT(`description`, '&lt;p&gt;&lt;strong&gt;Edit at ', UTC_TIMESTAMP(), ' UTC&lt;&#x2F;p&gt;&lt;&#x2F;strong&gt;', '{{add_info}}') WHERE id = {{id}};", obj);

	return db.update(sql);
};
exports.updateForBuyAuctionNow = function(auc_id, acc_id) {
	var now = new Date().toISOString().slice(0, 19).replace('T', ' ');
	var obj = {
		'id': auc_id,
		'has-winner': true,
		'highest-id': acc_id,
		'end-date': now
	};

	var sql = mustache.render("UPDATE `Auction` SET `has-winner` = {{has-winner}}, `highest-id` = '{{highest-id}}', " +
		"`end-date` = '{{end-date}}', `current-price` = `buyout-price` WHERE `id` = '{{id}}';",
		obj);

	return db.update(sql);
};
exports.updateForBidAuction_HigherThanMAX = function(auc_id, acc_id, NEW_MAX) {
	var obj = {
		'id': auc_id,
		'highest-id': acc_id,
		'MAX-PRICE': NEW_MAX
	};
	var sql = mustache.render("UPDATE `Auction` SET `current-price` = `MAX-PRICE` + `bid-increment`, `highest-id` = '{{highest-id}}', " +
		"`MAX-PRICE` = '{{MAX-PRICE}}', `has-buyout` = IF(`buyout-price` <= `current-price`, false, true) WHERE `id` = '{{id}}';",
		obj);

	return db.update(sql);
};
exports.updateForBidAuction_LowerThanMAX = function(auc_id, bid_price) {
	var obj = {
		'id': auc_id,
		'bid_price': bid_price
	};
	var sql = mustache.render("UPDATE `Auction` SET `current-price` = '{{bid_price}}' + `bid-increment`, " +
		"`has-buyout` = IF(`buyout-price` <= `current-price`, false, true) WHERE `id` = '{{id}}';",
		obj);

	return db.update(sql);
};
exports.updateForBidAuction_EqualToMAX = function(auc_id, bid_price) {
	var obj = {
		'id': auc_id,
		'bid_price': bid_price
	};
	var sql = mustache.render("UPDATE `Auction` SET `current-price` = '{{bid_price}}', " +
		"`has-buyout` = IF(`buyout-price` <= `current-price`, false, true) WHERE `id` = '{{id}}';",
		obj);

	return db.update(sql);
};
exports.checkIsNewAuction = function(auc_id) {
	var d = q.defer();

	var obj = {
		id: auc_id,
		min: constants.LIMIT_MINUTES_BE_NEW_AUCTION
	};

	var sql = mustache.render("SELECT COUNT(*) AS count FROM `Auction` " +
		"WHERE TIMESTAMPDIFF(minute,`start-date`,utc_timestamp()) < {{min}} AND `id` = '{{id}}';", obj);

	db.load(sql).then(function(rows) {
		var rlt = false;
		if (rows[0]["count"] > 0) rlt = true;
		d.resolve(rlt);

	}).fail(function(err) {
		d.reject(err);
	});

	return d.promise;
};
exports.checkIsAuctionOwner = function(auc_id, acc_id) {
	var d = q.defer();

	var obj = {
		'id': auc_id,
		'seller-id': acc_id
	};

	var sql = mustache.render("SELECT COUNT(*) AS count FROM `Auction` " +
		"WHERE `id` = '{{id}}' AND `seller-id` = '{{seller-id}}';", obj);

	db.load(sql).then(function(rows) {
		var rlt = false;
		if (rows[0]["count"] > 0) rlt = true;
		d.resolve(rlt);

	}).fail(function(err) {
		d.reject(err);
	});

	return d.promise;
};