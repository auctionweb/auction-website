var mustache = require('mustache'),
	q = require('q'),
	CryptoJS = require('crypto-js');
var db = require('../fn/db');

exports.loadAll = function() {
	var sql = "SELECT *, DATE_FORMAT(`expired-date`,'%Y-%m-%d %h:%i:%s') AS `expired-date-string` FROM `Account`;";
	return db.load(sql);
};

exports.loadDetail = function(id) {
	var d = q.defer();

	var obj = {
		id: id
	};

	var sql = mustache.render(
		"SELECT *, DATE_FORMAT(`expired-date`,'%Y-%m-%d %h:%i:%s') AS `expired-date-string` FROM `Account` WHERE `id` = '{{id}}';",
		obj
	);

	db.load(sql).then(function(rows) {
		d.resolve(rows[0]);
	}).fail(function(err) {
		d.reject(err);
	});

	return d.promise;
}

exports.update = function(entity) {
	var sql = mustache.render(
		"UPDATE `Account` SET `hashpass` = '{{password}}' WHERE `id` = '{{id}}';",
		entity
	);

	return db.update(sql);
}

exports.delete = function(entity) {
	var sql = mustache.render(
		"DELETE FROM `account` WHERE `id` = '{{id}}';",
		entity
	);
	console.log(sql);
	return db.delete(sql);
}


//________________OTHER_____________________
exports.updateBasicInfo = function(entity) {
	var sql = mustache.render(
		"UPDATE `Account` SET `fullname` = '{{fullname}}', `gender` = '{{gender}}', `dob` = '{{dob}}', " +
		"`phone-number` = '{{phone-number}}', `address` = '{{address}}' WHERE `id` = '{{id}}';",
		entity
	);
	return db.update(sql);
}

exports.updatePassword = function(id, new_pass) {
	var hashpass = CryptoJS.MD5("" + new_pass);
	var sql = mustache.render(
		"UPDATE `Account` SET `hashpass` = '{{hashpass}}' WHERE `id` = '{{id}}';", {
			id: id,
			hashpass
		}
	);
	console.log(sql);
	return db.update(sql);
};
exports.updateEmail = function(id, new_email) {
	var sql = mustache.render(
		"UPDATE `Account` SET `verified` = IF(`email` = '{{email}}', `verified`, false), `email` = '{{email}}' WHERE `id` = '{{id}}';", {
			id: id,
			email: new_email
		}
	);
	console.log(sql);
	return db.update(sql);
};

exports.getEmail = function(id) {
	var d = q.defer();
	var obj = {
		id: id
	};
	var sql = mustache.render(
		"SELECT `email` FROM `Account` WHERE `id` = '{{id}}';",
		obj
	);
	console.log(sql);
	db.load(sql).then(function(rows) {
		d.resolve(rows[0]);
	}).fail(function(err) {
		d.reject(err);
	});
	return d.promise;
};

exports.checkIfPasswordCorrect = function(id, password) {
	var d = q.defer();

	var hashpass = CryptoJS.MD5(password);
	var sql = mustache.render(
		"SELECT COUNT(*) AS `count` FROM `Account` WHERE `id` = '{{id}}' AND `hashpass` = '{{hashpass}}';", {
			id: id,
			hashpass: hashpass
		}
	);
	console.log(sql);
	db.load(sql).then(function(rows) {
		var rlt = false;
		if (rows[0]["count"] > 0) rlt = true;
		console.log(rlt);
		d.resolve(rlt);

	}).fail(function(error) {
		console.log(error);
		d.reject(error);
	});

	return d.promise;
};
exports.checkIfCanCreateAuction = function(id) {
	var d = q.defer();

	var sql = mustache.render(
		"SELECT COUNT(*) AS `count` FROM `Account` " +
		"WHERE `id` = '{{id}}' AND `level` > 0 AND `expired-date` > UTC_TIME();", {
			id: id
		}
	);
	console.log(sql);
	db.load(sql).then(function(rows) {
		var rlt = false;
		if (rows[0]["count"] > 0) rlt = true;
		console.log(rlt);
		d.resolve(rlt);

	}).fail(function(error) {
		console.log(error);
		d.reject(error);
	});

	return d.promise;
};

exports.createAccount = function(info) {
	info.level = 0;
	info.verified = false;
	info.hashpass = CryptoJS.MD5(info.password);
	var sql = mustache.render('INSERT INTO Account(id, email, hashpass, level, verified, fullname) ' +
		'values("{{username}}", "{{email}}", "{{hashpass}}", {{level}}, {{verified}}, "{{fullname}}");', info);

	console.log(sql);
	return db.insert(sql);
};

exports.checkAccountLoginInfo = function(info) {
	var d = q.defer();
	var result = {};
	result.success = false;

	var sql = mustache.render('SELECT id, fullname, hashpass, level FROM Account WHERE id = "{{username}}";', info);
	db.load(sql).then(function(rows) {
			if (rows.length == 1) {
				var hashpass = CryptoJS.MD5(info.password);
				if (hashpass == rows[0].hashpass) {
					result.success = true;
					result.userID = rows[0].id;
					result.fullname = rows[0].fullname;
					result.level = rows[0].level;
				} else {
					result.success = false;
					result.msg = 'Password incorrect!';
				}
			} else {
				result.success = false;
				result.msg = 'Username does not exist!';
			}
			d.resolve(result);
		})
		.fail(function(error) {
			console.log(error);
			d.reject(error);
		});

	return d.promise;
}