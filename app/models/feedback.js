var mustache = require('mustache'),
	q = require('q');
var db = require('../fn/db'),
	constants = require('../fn/constants');

exports.loadDetail = function(auc_id, from_acc_id) {
	var d = q.defer();

	var obj = {
		"auction-id": auc_id,
		"from": from_acc_id
	};

	var sql = mustache.render(
		"SELECT f.*, a.`cat-id` AS `cat-id`, a.`title` AS `auction-title`, DATE_FORMAT(`date`,'%Y-%m-%d %h:%i:%s') AS `date-string`" +
		"FROM `Auction-Feedback` AS `f`, `Auction` AS `a` " +
		"WHERE f.`auction-id` = a.`id` AND f.`auction-id` = '{{auction-id}}' AND f.`from` = '{{from}}';",
		obj
	);

	db.load(sql).then(function(rows) {
		d.resolve(rows[0]);
	}).fail(function(err) {
		d.reject(err);
	});

	return d.promise;
}
exports.loadAll_ByToAccountID_OrderByDateDESC = function(to_acc_id) {
	var obj = {
		"to": to_acc_id
	};
	var sql = mustache.render("SELECT f.*, a.`cat-id` AS `cat-id`, a.`title` AS `auction-title`, DATE_FORMAT(`date`,'%Y-%m-%d %h:%i:%s') AS `date-string` " +
		"FROM `Auction-Feedback` AS `f`, `Auction` AS `a` " +
		"WHERE f.`auction-id` = a.`id` AND f.`to` = '{{to}}' " +
		"ORDER BY `date` DESC;",
		obj
	);
	return db.load(sql);
};

exports.countAll_ByToAccountID = function(to_acc_id) {
	var d = q.defer();

	var obj = {
		"to": to_acc_id
	};
	var sql = mustache.render("SELECT COUNT(*) AS count FROM `Auction-Feedback` " +
		"WHERE `to` = '{{to}}';",
		obj
	);
	db.load(sql).then(function(rows) {
		d.resolve(rows[0]["count"]);
	}).fail(function(err) {
		d.reject(err);
	});

	return d.promise;
};
exports.countAll_ByToAccountID_PointPlus = function(to_acc_id) {
	var d = q.defer();

	var obj = {
		"to": to_acc_id,
		"point": 1
	};
	var sql = mustache.render("SELECT COUNT(*) AS count FROM `Auction-Feedback` " +
		"WHERE `to` = '{{to}}' AND `point` = '{{point}}';",
		obj
	);

	db.load(sql).then(function(rows) {
		d.resolve(rows[0]["count"]);
	}).fail(function(err) {
		d.reject(err);
	});

	return d.promise;
};
exports.countAll_ByToAccountID_PointMinus = function(to_acc_id) {
	var d = q.defer();

	var obj = {
		"to": to_acc_id,
		"point": -1
	};
	var sql = mustache.render("SELECT COUNT(*) AS count FROM `Auction-Feedback` " +
		"WHERE `to` = '{{to}}' AND `point` = '{{point}}';",
		obj
	);

	db.load(sql).then(function(rows) {
		d.resolve(rows[0]["count"]);
	}).fail(function(err) {
		d.reject(err);
	});

	return d.promise;
};


exports.insert = function(entity) {
	entity['date'] = entity['date'].toISOString().slice(0, 19).replace('T', ' ');
	var sql = mustache.render(
		"INSERT INTO `Auction-Feedback` (`auction-id`, `from`, `to`, `from-type`, `point`, `comment`, `date`) VALUES " +
		"('{{auction-id}}', '{{from}}', '{{to}}', '{{from-type}}', '{{point}}', '{{comment}}', '{{date}}' );",
		entity
	);

	return db.insert(sql);
};

exports.delete = function(entity) {
	var sql = mustache.render(
		"DELETE FROM `Auction-Feedback` WHERE `auction-id` = '{{auction-id}}' AND `from` = '{{from}}';",
		entity
	);

	return db.delete(sql);
};

exports.checkIfExist = function(auc_id, from_acc_id) {
	var d = q.defer();

	var obj = {
		'auction-id': auc_id,
		'from': from_acc_id
	};

	var sql = mustache.render(
		"SELECT COUNT(*) as count FROM `Auction-Feedback` WHERE `auction-id` = '{{auction-id}}' AND `from` = '{{from}}';",
		obj
	);

	db.load(sql).then(function(rows) {
		var rlt = false;
		if (rows[0]["count"] > 0) rlt = true;
		d.resolve(rlt);

	}).fail(function(err) {
		d.reject(err);
	});

	return d.promise;
};