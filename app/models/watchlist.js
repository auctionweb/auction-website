var mustache = require('mustache'),
	q = require('q');
var db = require('../fn/db');

exports.insert = function(auc_id, acc_id) {
	var obj = {
		'auction-id': auc_id,
		'account-id': acc_id
	};

	var sql = mustache.render(
		"INSERT INTO `Auction-Watchlist` (`auction-id`, `account-id`) VALUES ('{{auction-id}}', '{{account-id}}');",
		obj
	);

	return db.insert(sql);
};

exports.delete = function(auc_id, acc_id) {
	var obj = {
		'auction-id': auc_id,
		'account-id': acc_id
	};

	var sql = mustache.render(
		"DELETE FROM `Auction-Watchlist` WHERE `auction-id` = '{{auction-id}}' AND `account-id` = '{{account-id}}';",
		obj
	);

	return db.delete(sql);
};

exports.checkIfExist = function(auc_id, acc_id) {
	var d = q.defer();

	var obj = {
		'auction-id': auc_id,
		'account-id': acc_id
	};

	var sql = mustache.render(
		"SELECT COUNT(*) as count FROM `Auction-Watchlist` WHERE `auction-id` = '{{auction-id}}' AND `account-id` = '{{account-id}}';",
		obj
	);

	db.load(sql).then(function(rows) {
		var rlt = false;
		if (rows[0]["count"] > 0) rlt = true;
		d.resolve(rlt);

	}).fail(function(err) {
		d.reject(err);
	});

	return d.promise;
};