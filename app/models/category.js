var mustache = require('mustache'),
	q = require('q');
var db = require('../fn/db');

exports.loadAll = function() {
	var sql = "SELECT * FROM `Auction-Category`;";
	return db.load(sql);
};

exports.loadDetail = function(id) {
	var d = q.defer();

	var obj = {
		id: id
	};

	var sql = mustache.render(
		"SELECT * FROM `Auction-Category` WHERE `id` = '{{id}}';",
		obj
	);

	db.load(sql).then(function(rows) {
		d.resolve(rows[0]);
	}).fail(function(err) {
		d.reject(err);
	});

	return d.promise;
}

exports.insert = function(entity) {
	var sql = mustache.render(
		"INSERT INTO `Auction-Category` (`name`, `description`) VALUES ('{{name}}', '{{description}}');",
		entity
	);

	return db.insert(sql);
}

exports.update = function(entity) {
	var sql = mustache.render(
		"UPDATE `Auction-Category` SET `name` = '{{name}}', `description` = '{{description}}' WHERE `id` = '{{id}}';",
		entity
	);

	return db.update(sql);
}

exports.delete = function(entity) {
	var sql = mustache.render(
		"DELETE FROM `Auction-Category` WHERE `id` = '{{id}}';",
		entity
	);

	return db.delete(sql);
}