var mustache = require('mustache'),
	q = require('q'),
	CryptoJS = require('crypto-js');
var db = require('../fn/db'),
	constants = require('../fn/constants');

exports.loadAll = function() {
	var sql = "SELECT sellrequest.`account-id`, account.fullname ,content,	" +
		"DATE_FORMAT(sellrequest.`date`,'%Y-%m-%d %h:%i:%s') AS `date_string` FROM 	`account-sellrequest` sellrequest LEFT JOIN account ON sellrequest.`account-id` = account.id WHERE 	sellrequest.status = 'waiting' ORDER BY sellrequest.date DESC;";
	return db.load(sql);
};

exports.updateToSellAccount = function(entity) {
	entity.plus_minus = constants.ACCOUNT_SELL_REQUEST_PLUS_MINUTE;
	entity.level = constants.ACCOUNT_TYPE_SELLER;
	var sql = mustache.render(
		"UPDATE `account` account, `account-sellrequest` sellrequest SET sellrequest.`status` = 'accepted', account.`level` = {{level}}, " +
		"account.`expired-date` = DATE_ADD(utc_time(), INTERVAL {{plus_minus}} MINUTE) " +
		"WHERE account.`id` = sellrequest.`account-id` AND sellrequest.`account-id` = '{{accountIdAccept}}';",
		entity
	);
	console.log(sql);
	return db.update(sql);
};

exports.rejectRequest = function(entity) {
	var sql = mustache.render(
		"UPDATE `account-sellrequest` sellrequest SET sellrequest.`status` = 'rejected' WHERE sellrequest.`account-id` = '{{accountIdReject}}';",
		entity
	);
	console.log(sql);
	return db.update(sql);
};

exports.insert = function(entity) {
	var sql = mustache.render(
		"INSERT INTO `Account-SellRequest` (`account-id`, `date`, `content`, `status`) VALUES " +
		"('{{account-id}}', '{{date}}', '{{content}}', '{{status}}');",
		entity
	);

	return db.insert(sql);
}