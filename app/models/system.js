var mustache = require('mustache'),
	q = require('q');
var db = require('../fn/db'),
	constants = require('../fn/constants'),
	auctionRepo = require('../models/auction'),
	bidloggerRepo = require('../models/bidlogger'),
	accountRepo = require('../models/account');

var _this = this;

exports.getInfo_ToSendMail_MakeBidEvent = function(auc_id) {
	var d = q.defer();

	q.all([
		_this.getSellerEmail(auc_id),
		_this.getHighestBidderEmail(auc_id),
		_this.getBeforeHighestBidderEmail(auc_id),
		auctionRepo.loadDetail(auc_id)

	]).spread(function(seller, highest, before, auction) {
		var info = {
			seller: seller,
			highest: highest,
			before: before,
			auction: auction
		};
		d.resolve(info);

	}).fail(function(err) {
		d.reject(err);
	});

	return d.promise;
};
exports.getInfo_ToSendMail_KickOutEvent = function(auc_id, acc_id) {
	var d = q.defer();

	q.all([
		accountRepo.loadDetail(acc_id),
		auctionRepo.loadDetail(auc_id)

	]).spread(function(account, auction) {
		var info = {
			email: account['email'],
			auction: auction
		};

		d.resolve(info);

	}).fail(function(err) {
		d.reject(err);
	});

	return d.promise;
};
exports.updateAuction_WhenKickHighestBidder = function(auc_id) {
	var d = q.defer();

	q.all([
		bidloggerRepo.loadHighestDetail(auc_id),
		auctionRepo.loadDetail(auc_id)
	]).spread(function(biglog, auction) {
		if (!biglog) d.resolve();

		auction['highest-id'] = biglog['account-id'];
		auction['current-price'] = biglog['price'];
		auction['MAX-PRICE'] = biglog['price'];
		auctionRepo.update(auction).then(function() {
			d.resolve();
		}).fail(function(err) {
			console.log(err);
			d.reject(err);
		});
	}).fail(function() {
		console.log(err);
		d.reject(err);
	});

	return d.promise;

};
exports.updateAuctionEndTime_AutoExtend = function(auc_id) {
	var obj = {
		id: auc_id,
		min_before_end: constants.AUCTION_AUTO_EXTEND_BEFORE_END_MIN,
		min_extend: constants.AUCTION_AUTO_EXTEND_EXTEND_MIN
	};
	var sql = mustache.render("UPDATE `Auction` as A, (SELECT COUNT(*) as `count` FROM `Auction-BidLogger` WHERE `auction-id` = '{{id}}') as B  " +
		"SET A.`end-date` = DATE_ADD(A.`end-date`, INTERVAL {{min_extend}} MINUTE), A.`plus-minute` = 0 " +
		"WHERE A.`end-date` > UTC_TIMESTAMP() AND A.`plus-minute` > 0 " +
		"AND TIMESTAMPDIFF(MINUTE, UTC_TIMESTAMP(), A.`end-date`) < {{min_before_end}} AND A.`id` = '{{id}}';",
		obj
	);
	return db.update(sql);
};

exports.getBeforeHighestBidderEmail = function(auc_id, highest) {
	var d = q.defer();

	var obj = {
		"auction-id": auc_id,
		highest: highest
	};
	var sql = mustache.render("SELECT A.`email` " +
		"FROM `Account` as A, `Auction-BidLogger` as B " +
		"WHERE A.`id` = B.`account-id` AND B.`auction-id` = '{{auction-id}}' AND B.`account-id` <> '{{highest}}' " +
		"ORDER BY B.`date` DESC, B.`price` DESC LIMIT 1,1;",
		obj
	);
	console.log(sql);
	db.load(sql).then(function(rows) {
		if (rows.length <= 0) return d.resolve(null);
		d.resolve(rows[0]["email"]);

	}).fail(function(err) {
		console.log(err);
		d.reject(err);
	});
	return d.promise;
};
exports.getHighestBidderEmail = function(auc_id) {
	var d = q.defer();

	var obj = {
		"auction-id": auc_id
	};
	var sql = mustache.render(" SELECT Ac.`email` FROM `Auction` AS Au, `Account` AS Ac " +
		"WHERE Au.`highest-id` = Ac.`id` AND Au.`id` = '{{auction-id}}';",
		obj
	);
	db.load(sql).then(function(rows) {
		if (rows.length <= 0) return d.resolve(null);
		d.resolve(rows[0]["email"]);

	}).fail(function(err) {
		console.log(err);
		d.reject(err);
	});
	return d.promise;
};
exports.getSellerEmail = function(auc_id) {
	var d = q.defer();

	var obj = {
		"auction-id": auc_id
	};
	var sql = mustache.render(" SELECT Ac.`email` FROM `Auction` AS Au, `Account` AS Ac " +
		"WHERE Au.`seller-id` = Ac.`id` AND Au.`id` = '{{auction-id}}';",
		obj
	);
	db.load(sql).then(function(rows) {
		if (rows.length <= 0) return d.resolve(null);
		d.resolve(rows[0]["email"]);

	}).fail(function(err) {
		console.log(err);
		d.reject(err);
	});
	return d.promise;
};