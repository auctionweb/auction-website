var q = require('q'),
	mkdirp = require('mkdirp'),
	multer = require('multer'),
	cloudinary = require('cloudinary'),
	fs = require('fs');
var constants = require("./constants"),
	db = require('../fn/db'),
	auctionRepo = require('../models/auction'),
	bidloggerRepo = require('../models/bidlogger'),
	feedbackRepo = require('../models/feedback'),
	watchlistRepo = require('../models/watchlist');

var _this = this;
//______________________________________________________
exports.formatString = function(str) {
	var patt = /[ ]{2,}/g;
	str = str.trim();
	return str.replace(patt, " ");
};
exports.formatDate = function(date) {
	var d = new Date(date),
		month = '' + (d.getMonth() + 1),
		day = '' + d.getDate(),
		year = d.getFullYear();

	if (month.length < 2) month = '0' + month;
	if (day.length < 2) day = '0' + day;

	return [year, month, day].join('-');
};
//xxx
exports.convertStringToWords = function(str) {
	var new_str = str.trim();
	new_str = new_str.replace(/[.'*+?^${}()|[\]\\]/g, " ");
	var rlt = new_str.split(/[ ]{1,}/g);
	return rlt.filter(function(w) {
		return w != "";
	});
};
exports.makeDir = function(path) {
	var d = q.defer();

	mkdirp(path, function(err) {
		if (err) d.reject(err);
		else d.resolve();
	});
	return d.promise;

};

//____________________AUCTION_________________________________
const DEFAULT_AUCTION_INFO = {
	"seller-id": constants.AUCTION_USER_ID_DEFAULT,
	"plus-minute": 10,
	"auto-support": constants.AUCTION_AUTO_SUPPORT_DEFAULT,
	"title": "default",
	"description": "default",
	"start-date": "2017-06-10 12:00:00",
	"end-date": "2017-06-10 12:00:00",
	"start-price": 0,
	"bid-increment": 0,
	"has-buyout": false,
	"buyout-price": 0,
	"highest-id": constants.AUCTION_USER_ID_DEFAULT,
	"cat-id": constants.AUCTION_IDCATEGORY_DEFAULT,
	"current-price": 0,
	"has-winner": false,
	"MAX-PRICE": 0
};

exports.parseCreateAuctionFormToInfo = function(req) {
	var form = req.body;
	var now = new Date();
	var has_buyout = (form.has_buyout_price.indexOf("yes") > -1) ? true : false;
	var info = {
		"seller-id": req.session.userID,
		"plus-minute": form.plus_minute,
		"auto-support": constants.AUCTION_AUTO_SUPPORT_DEFAULT,
		"title": form.auc_title,
		"description": form.auc_desc,
		"start-date": now,
		"end-date": new Date(form.date_end_string),
		"start-price": form.price_start,
		"bid-increment": form.bid_increment,
		"has-buyout": has_buyout,
		"buyout-price": form.price_buyout,
		"highest-id": constants.AUCTION_USER_ID_DEFAULT,
		"cat-id": form.auc_category,
		"current-price": form.price_start,
		"has-winner": false,
		"MAX-PRICE": form.price_start
	};
	return info;
};
exports.createDefaultAuction = function() {
	var d = q.defer();

	auctionRepo.insert(DEFAULT_AUCTION_INFO)
		.then(function(id) {
			d.resolve(id);
		}).fail(function(err) {
			d.reject(err);
		});

	return d.promise;
};
exports.getAuctionBidNumber = function(auc_id) {
	var d = q.defer();
	bidloggerRepo.loadAll_ByAuctionID_OrderByDateDESC(auc_id)
		.then(function(rows) {
			d.resolve(rows.length);
		})
		.fail(function(err) {
			console.log(err);
			d.reject(err);
		});
	return d.promise;
};
exports.encodeBidderID = function(id) {
	var n = id.length;
	return "****" + id[n - 2] + id[n - 1];
};
exports.parseInfoForListAuctionView = function(rows, acc_id, img_max_width, img_max_height) {
	var d = q.defer();

	var promises = rows.map(function(row, index) {
		return parseInfoForListAuctionView_OneCell(row, acc_id, img_max_width, img_max_height).then(function(cell) {
			return {
				index: index,
				cell: cell
			};
		}).fail(function(err) {
			d.reject(err);
		});
	});

	q.all(promises).then(function(results) {
		//sap xep cho dung thu tu
		for (var i = 0; i < results.length - 1; i++) {
			for (var j = i + 1; j < results.length; j++) {
				if (results[i].index > results[j].index) {
					var temp = results[i];
					results[i] = results[j];
					results[j] = temp;
				}
			}
		}

		var cells = [];
		for (var i = 0; i < results.length; i++) {
			cells.push(results[i].cell);
		}
		d.resolve(cells);
	}).fail(function(err) {
		d.reject(err);
	});

	return d.promise;
};
var parseInfoForListAuctionView_OneCell = function(row, acc_id, img_max_width, img_max_height) {
	var d = q.defer();

	var obj = {};
	obj.auc_id = row["id"];
	obj.auc_title = row["title"];
	obj.cat_id = row["cat-id"];
	obj.seller_id = row["seller-id"];
	obj.end_date_string = row["end-date-string"];
	obj.current_price = row["current-price"];
	obj.buyout_price = row["buyout-price"];
	obj.has_buyout = row["has-buyout"];

	q.all([
		_this.getListHTMLImage(obj.auc_id, img_max_width, img_max_height),
		_this.getAuctionBidNumber(obj.auc_id),
		auctionRepo.checkIsNewAuction(obj.auc_id),
		watchlistRepo.checkIfExist(obj.auc_id, acc_id)
	]).spread(function(imgs, bids, isNew, isInWatchlist) {
		obj.isNew = isNew;
		obj.auc_image = imgs[0];
		obj.isInWatchlist = isInWatchlist;

		obj.auc_bids = bids;
		obj.has_bidder = false;
		if (bids > 0) {
			obj.has_bidder = true;
			obj.auc_highest = _this.encodeBidderID(row["highest-id"]);
			if (row["highest-id"] == acc_id) obj.isYou = true;
			obj.auc_highest_full_id = row["highest-id"];
		}
		d.resolve(obj);
	}).fail(function(err) {
		console.log("BID INFO: " + err);
		d.reject(err);
	});
	return d.promise;
};

exports.buyAuctionNow = function(auc_id, acc_id) {
	var d = q.defer();

	auctionRepo.loadDetail(auc_id).then(function(auction) {
		if (!auction) d.resolve(false);
		if (auction['has-winner'] || auction['buyout_price'] < auction['current-price']) {
			d.resolve(false);
		}

		var buyout_price = auction['buyout-price'];
		auctionRepo.updateForBuyAuctionNow(auc_id, acc_id).then(function(changedRows) {
			if (changedRows <= 0) {
				d.resolve(false);
			} else {
				_this.createBidLogger(auc_id, acc_id, buyout_price).then(function() {
					d.resolve(true);
				}).fail(function(err) {
					console.log(err);
					d.reject(err);
				});
			}
		}).fail(function(err) {
			d.reject(err);
		});
	}).fail(function(err) {
		d.reject(err);
	});
	return d.promise;
};
exports.bidAuction = function(auc_id, acc_id, bid_price) {
	var d = q.defer();

	//rlt = {success, hasHigher}
	var rlt = {};
	rlt.success = false;
	rlt.hasHigher = false;

	auctionRepo.loadDetail(auc_id).then(function(auction) {

		if (!auction) {
			d.resolve(rlt);
		} else if (auction['end-date'] < Date() || bid_price <= auction['current-price']) { //rollback
			d.resolve(rlt);
		} else {
			var bid_increment = auction['bid-increment'],
				MAX = auction['MAX-PRICE'],
				highest_id = auction['highest-id'];


			var type, func;
			if (bid_price < MAX) {
				func = auctionRepo.updateForBidAuction_LowerThanMAX(auc_id, bid_price);
				type = 0;
			} else if (bid_price == MAX) {
				func = auctionRepo.updateForBidAuction_EqualToMAX(auc_id, bid_price);
				type = 1;
			} else {
				func = auctionRepo.updateForBidAuction_HigherThanMAX(auc_id, acc_id, bid_price);
				type = 2;
			}

			func.then(function(changedRows) {
				if (changedRows <= 0) {
					d.resolve(rlt);
				} else {
					switch (type) {
						case 0:
							_this.createBidLogger(auc_id, acc_id, bid_price).then(function(id1) {
								_this.createBidLogger(auc_id, highest_id,
										parseFloat(bid_price) + parseFloat(bid_increment))
									.then(function(id2) {
										if (acc_id == highest_id)
											rlt.success = true;
										else
											rlt.hasHigher = true;
										d.resolve(rlt);
									}).fail(function(err) {
										bidloggerRepo.delete(id1);
										d.reject(err);
									});
							}).fail(function(err) {
								d.reject(err);
							});
							break;
						case 1:
							_this.createBidLogger(auc_id, acc_id, bid_price).then(function(id1) {
								_this.createBidLogger(auc_id, highest_id, bid_price)
									.then(function(id2) {
										if (acc_id == highest_id)
											rlt.success = true;
										else
											rlt.hasHigher = true;
										d.resolve(rlt);
									}).fail(function(err) {
										bidloggerRepo.delete(id1)
										d.reject(err);
									})
							}).fail(function(err) {
								d.reject(err);
							});
							break;
						case 2:
							_this.createBidLogger(auc_id, acc_id, parseFloat(MAX) + parseFloat(bid_increment))
								.then(function(id1) {
									rlt.success = true;
									d.resolve(rlt);
								}).fail(function(err) {
									d.reject(err);
								});

							break;
					}
				}
			}).fail(function(err) {
				d.reject(err);
			});
		}

	}).fail(function(err) {
		d.reject(err);
	});

	return d.promise;
};

//__________________ACCOUNT_________________________________
exports.countAccountTotalPoint = function(acc_id) {
	var d = q.defer();

	if (acc_id == constants.AUCTION_USER_ID_DEFAULT) d.resolve(0);
	else {

		q.all([
			feedbackRepo.countAll_ByToAccountID_PointPlus(acc_id),
			feedbackRepo.countAll_ByToAccountID_PointMinus(acc_id)
		]).spread(function(num_of_plus, num_of_minus) {
			d.resolve(num_of_plus * constants.POINT_PLUS + num_of_minus * constants.POINT_MINUS);
		}).fail(function(err) {
			d.reject(err);
		});
	}
	return d.promise;
};

exports.getAccountPointInfo = function(acc_id) {
	var d = q.defer();

	var info = {
		num_of_plus: 0,
		num_of_minus: 0,
		num_of_all: 0,
		percent: 100
	};

	q.all([
		feedbackRepo.countAll_ByToAccountID(acc_id),
		feedbackRepo.countAll_ByToAccountID_PointPlus(acc_id),
		feedbackRepo.countAll_ByToAccountID_PointMinus(acc_id)
	]).spread(function(all, plus, minus) {
		console.log(all + " - " + plus + " - " + minus);
		if (all == 0) d.resolve(info);

		info.num_of_plus = plus;
		info.num_of_minus = minus;
		info.num_of_all = all;
		info.percent = (100 * ((plus * constants.POINT_PLUS) - (minus * constants.POINT_MINUS))) / all;
		d.resolve(info);

	}).fail(function(err) {
		d.reject(err);
	});
	return d.promise;
};

//__________________BIGLOGGER_________________________________
exports.createBidLogger = function(auc_id, acc_id, price) {
	var d = q.defer();

	var now = new Date().toISOString().slice(0, 19).replace('T', ' ');
	var info = {
		'auction-id': auc_id,
		'account-id': acc_id,
		'date': now,
		'price': price
	};

	bidloggerRepo.insert(info)
		.then(function(id) {
			d.resolve(id);
		}).fail(function(err) {
			d.reject(err);
		});

	return d.promise;
};


//_________________IMAGE UPLOAD___________________________________



//_________________IMAGE UPLOAD & GET___________________________________
var storage = multer.diskStorage({
	destination: function(req, file, callback) {
		var path = constants.TEMP_DIR_PATH;
		callback(null, path);
	},
	filename: function(req, file, callback) {
		var name = file.fieldname + "_" + req.session.userID + "_" + new Date().getTime();
		callback(null, name);
	}
});
var upload = multer({
	storage: storage
}).fields([{
	name: 'image1',
	maxCount: 1
}, {
	name: 'image2',
	maxCount: 1
}, {
	name: 'image3',
	maxCount: 1
}]);

cloudinary.config({
	cloud_name: 'ah',
	api_key: '483322462727681',
	api_secret: 'YZNfd9aBPNp3-Rd19PK1jMOpd1E'
});

exports.uploadToCDN = function(req, res, folder) {
	var d = q.defer();
	upload(req, res, function(err) {
		if (err) {
			console.log("ERROR uploading: " + err);
			d.reject(err);
		}
		var field1 = req.files['image1'],
			field2 = req.files['image2'],
			field3 = req.files['image3'];
		var files = [];
		if (field1) files.push(field1[0]);
		if (field2) files.push(field2[0]);
		if (field3) files.push(field3[0]);

		files.forEach(function(file) {
			if (file) {
				var path = file.destination + "/" + file.filename;
				var publicID = constants.CDN_UPLOAD_DIR_PATH + "/" +
					folder + "/" + file.fieldname;
				cloudinary.uploader.upload(path, function(rlt) {
					fs.unlink(path, function(err) {
						if (err) console.log("DELETE FILE ERROR: " + err);
						else console.log(" -- deleted: " + path);
					});
				}, {
					public_id: publicID
				});
			}
		});

		d.resolve();
	});

	return d.promise;
};

exports.getListHTMLImage = function(auc_id, width, height) {
	var d = q.defer();

	var rlt = [];
	var prefix = constants.CDN_UPLOAD_DIR_PATH + "/" +
		auc_id + "/";
	cloudinary.api.resources(function(result) {
		for (var i = 0; i < result["resources"].length; i++) {
			var img = result["resources"][i];

			//var x = {};
			var html = cloudinary.image(img["public_id"], {
				width: width,
				height: height,
				crop: "fit",
			});
			//console.log("HTML: " + x);
			rlt.push(html);
		}
		d.resolve(rlt);

	}, {
		type: "upload",
		prefix: prefix
	});

	return d.promise;
};