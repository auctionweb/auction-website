var define = require("node-constants")(exports);
// define is a function that binds "constants" to an object (commonly exports)

// a single constant
define("PI", 3.14);


//auction image upload
define({
	TEMP_DIR_PATH: "./temp",
	CDN_UPLOAD_DIR_PATH: "AuctionX",
	DATABASE: "AuctionDBX",
});

//auction
define({
	AUCTION_USER_ID_DEFAULT: "default",
	AUCTION_IDCATEGORY_DEFAULT: 17,
	AUCTION_AUTO_SUPPORT_DEFAULT: true,
	LIMIT_MINUTES_BE_NEW_AUCTION: 2,

	IMAGE_HEIGHT_AUCTION_DETAIL: 400,
	IMAGE_HEIGHT_AUCTION_CELL: 200,
	IMAGE_HEIGHT_HOME_PAGE: 500,

	AUCTION_AUTO_EXTEND_EXTEND_MIN: 10,
	AUCTION_AUTO_EXTEND_BEFORE_END_MIN: 5,

	TOP_AUCTION_NUMBER: 5,
	NUM_AUCTION_PER_PAGE: 6,

	ORDER_END_ASC: "end_asc",
	ORDER_END_DESC: "end_desc",
	ORDER_PRICE_ASC: "price_asc",
	ORDER_PRICE_DESC: "price_desc"

});

//account
define({
	ACCOUNT_GENDER_MALE: "male",
	ACCOUNT_GENDER_FEMALE: "female",
	ACCOUNT_TYPE_USER: 0,
	ACCOUNT_TYPE_SELLER: 1,
	ACCOUNT_TYPE_ADMIN: 2,
	ACCOUNT_SELL_REQUEST_PLUS_MINUTE: 5
});

//account-sellrequest
define({
	SELLREQUEST_STATUS_WAITING: "waiting",
	SELLREQUEST_STATUS_ACCEPTED: "accepted",
	SELLREQUEST_STATUS_REJECTED: "rejected",
});

//feedback
define({
	POINT_PLUS: 1,
	POINT_MINUS: 1,
	FROM_TYPE_SELLER: "seller",
	FROM_TYPE_WINNER: "winner",
	MIN_PERCENT_CAN_BID: 80
});