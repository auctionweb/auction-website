var express = require('express'),
	q = require('q'),
	mustache = require('mustache');

var helper = require('../fn/helper'),
	mailer = require('../fn/mailer'),
	bidloggerRepo = require('../models/bidlogger'),
	systemRepo = require('../models/system');

exports.handleMakeBidSuccessEvent = function(auc_id) {
	var d = q.defer();

	q.all([
		systemRepo.getInfo_ToSendMail_MakeBidEvent(auc_id),
		systemRepo.updateAuctionEndTime_AutoExtend(auc_id)
	]).spread(function(info, affectedRows) {
		console.log("system.handleMakeBidEvent - extend end time: " + affectedRows);

		info.auction['highest-id-encoded'] = helper.encodeBidderID(info.auction['highest-id']);
		var seller = {
				subject: "YOUR AUCTION PRICE HAS CHANGED",
				text: mustache.render("Auction <{{title}} - ID({{id}})> current price is <{{current-price}}$ - by: {{highest-id}}> now!", info.auction),
				html: ""
			},
			highest = {
				subject: "AUCTION - YOU ARE BIDDING - PRICE HAS CHANGED",
				text: mustache.render("Auction <{{title}} - ID({{id}})> current price is {{current-price}}$ now! YOU are highest bidder!", info.auction),
				html: ""
			},
			before = {
				subject: "AUCTION - YOU ARE BIDDING - PRICE HAS CHANGED",
				text: mustache.render("Auction <{{title}} - ID({{id}})> current price is {{current-price}}$ - by: {{highest-id-encoded}} now!", info.auction),
				html: ""
			};

		seller.to = info.seller;
		highest.to = info.highest;
		before.to = info.before;
		var promises = [];
		if (info.highest != info.before) {
			promises.push(mailer.sendMail(before));
		}
		promises.push(mailer.sendMail(seller));
		promises.push(mailer.sendMail(highest));


		q.all(promises);
		d.resolve(true);
	}).fail(function(err) {
		d.reject(err);
	});

	return d.promise;
};
exports.handleBuySuccessEvent = function(auc_id) {
	var d = q.defer();

	q.all([
		systemRepo.getInfo_ToSendMail_MakeBidEvent(auc_id)
	]).spread(function(info, affectedRows) {
		console.log("system.handleBuyEven");

		info.detal['highest-id-encoded'] = helper.encodeBidderID(info.auction['highest-id']);
		var seller = {
				subject: "YOUR AUCTION HAS BEEN SOLD",
				text: mustache.render("Auction <{{title}} - ID({{id}})> has been sold! PRICE <{{current-price}}$> - by: {{highest-id}} !", info.auction),
				html: ""
			},
			highest = {
				subject: "AUCTION HAS BEEN SOLD FOR YOU",
				text: mustache.render("Auction <{{title}} - ID({{id}})> has been sold for YOU! PRICE <{{current-price}}$>!", info.auction),
				html: ""
			},
			before = {
				subject: "AUCTION - YOU ARE BIDDING - HAS BEEN SOLD",
				text: mustache.render("Auction <{{title}} - ID({{id}})> has been sold! PRICE <{{current-price}}$> - by: {{highest-id-encoded}} !", info.auction),
				html: ""
			};

		seller.to = info.seller;
		highest.to = info.highest;
		before.to = info.before;
		var promises = [];
		if (info.highest != info.before) {
			promises.push(mailer.sendMail(before));
		}
		promises.push(mailer.sendMail(seller));
		promises.push(mailer.sendMail(highest));


		q.all(promises);
		d.resolve(true);
	}).fail(function(err) {
		d.reject(err);
	});

	return d.promise;
};
exports.handleKickOutEvent = function(auc_id, acc_id, isHighest) {
	var d = q.defer();

	var promises = [];
	promises.push(systemRepo.getInfo_ToSendMail_KickOutEvent(auc_id, acc_id));
	if (isHighest) {
		promises.push(bidloggerRepo.deleteAll_By_AuctionID_And_AccountID(auc_id, acc_id));
		promises.push(systemRepo.updateAuction_WhenKickHighestBidder(auc_id));
	}
	console.log("handle kick out >>>");
	q.all(promises).spread(function(info, temp1, temp2) {

		var options = {
			to: info.email,
			subject: "YOU HAS BEEN KICKED OUT OF AUCTION",
			text: mustache.render("You can't took in Auction <{{title}} - ID({{id}})> anymore!", info.auction),
			html: ""
		};
		mailer.sendMail(options);
		d.resolve(true);
	}).fail(function(err) {
		d.reject(err);
	});

	return d.promise;
};